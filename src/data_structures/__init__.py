"""Les différentes structures de données."""

from .binary_search_tree import BinarySearchTree
from .fifo import Queue
from .stack import Stack