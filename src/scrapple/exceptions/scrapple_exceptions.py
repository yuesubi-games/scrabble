class ScrappleException(Exception):
    """Exceptions spécifiques au Scrapple."""

class ScrappleInvalidActionException(ScrappleException):
    """Exceptions lancée lors d'une action non valide."""

class ScrappleInvalidWordException(ScrappleInvalidActionException):
    """Exception lancée quand un des mots n'est pas valide."""

class ScrappleNoWordFormed(ScrappleInvalidActionException):
    """Exception lancée quand aucun mot n'est formé."""

class ScrappleUsingUnassignedJokerLetter(ScrappleInvalidActionException):
    """
    Exception lancée quand le joueur utilise une lettre joker sans définir la
    lettre qu'elle doit remplacer.
    """

class ScrappleLetterNotOwned(ScrappleInvalidActionException):
    """
    Exception lancée quand le joueur essaye de jouer une lettre qu'il n'a pas.
    """

class ScrappleLettersNotAlined(ScrappleInvalidActionException):
    """Exception lancée quand le joueur n'aligne pas ses lettres."""

class ScrappleLettersOverlapping(ScrappleInvalidActionException):
    """Exception lancée quand le joueur mes des lettres au dessus d'autres."""

class ScrappleLettersNotConnected(ScrappleInvalidActionException):
    """Exception lancée quand les lettres ne sont pas connectés par d'autres."""

class ScrappleWordNotConnected(ScrappleInvalidActionException):
    """
    Exception lancée quand les lettres sont isolées de celles déjà placées, ou
    du centre pour le premier tour.
    """