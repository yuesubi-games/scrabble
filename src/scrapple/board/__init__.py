"""The board module"""

from .board import Board
from .tile import Tile