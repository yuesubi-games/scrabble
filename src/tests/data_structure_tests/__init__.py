"""Tout les test des structures de données."""

from .bst_tests import TestBinarySearchTree
from .fifo_tests import TestQueue
from .stack_test import TestStack