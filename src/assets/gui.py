import os
import pygame


pygame.font.init()


GUI_UNIT: int = 28


################################################################################
# PLATEAU DE JEU
################################################################################

# Les couleurs des cellules du plateau avec (coefficient_lettre
# coefficient_mot)
BOARD_COLOR: dict[tuple[int, int], pygame.Color] = {
    (1, 1): pygame.Color(233, 216, 166),
    (2, 1): pygame.Color(10, 147, 150),
    (3, 1): pygame.Color(0, 95, 115),
    (1, 2): pygame.Color(187, 62, 3),
    (1, 3): pygame.Color(174, 32, 18),
}

BOARD_BORDER_COLOR: pygame.Color = pygame.Color(50, 50, 50)


################################################################################
# MAIN DU JOUEUR
################################################################################

DECK_LETTER_SPACING: int = 0


################################################################################
# LETTRES
################################################################################

# Le chemin de la police d'écriture
FIRACODE_BOLD_FONT_PATH = os.path.join(os.path.dirname(__file__), "..", "..",
    "res", "fonts", "FiraCode", "Fira_Code_Regular_Nerd_Font_Complete_Mono.ttf")

# La police d'écriture des caractères
LETTER_CHAR_FONT: pygame.font.Font = pygame.font.Font(FIRACODE_BOLD_FONT_PATH,
    18)
LETTER_POINT_FONT: pygame.font.Font = pygame.font.Font(FIRACODE_BOLD_FONT_PATH,
    10)

LETTER_COLOR: pygame.Color = pygame.Color(150, 150, 150)
LETTER_FONT_COLOR: pygame.Color = pygame.Color(0, 0, 0)
LETTER_BORDER_COLOR: pygame.Color = pygame.Color(50, 50, 50)
LETTER_BORDER_WIDTH: int = 1

LETTER_MOVING_TILT: float = 30.0