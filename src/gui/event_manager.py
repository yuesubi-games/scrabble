import pygame


class EventManager:
    def __init__(self) -> None:
        """Constructeur."""

        self._keys_pressed: dict[int, bool] = {}
        self._keys_released: dict[int, bool] = {}
        self._keys_down: dict[int, bool] = {}

        self._buttons_pressed: dict[int, bool] = {}
        self._buttons_released: dict[int, bool] = {}
        self._buttons_down: dict[int, bool] = {}

        # Position de la souris
        self.mouse_pos: pygame.Vector2 = pygame.Vector2(0, 0)
        self.delta_mouse_pos: pygame.Vector2 = pygame.Vector2(0, 0)

        # L'entrée alpha numérique donnée
        self.alpha_numeric: str = str()

        # Demande de quitter l'application
        self.quit: bool = False

    def is_key_pressed(self, key: int) -> bool:
        """
        Savoir si une touche est pressée.
        :param key: L'identifiant de la touche.
        :return: Vrai si la touche est pressée.
        """
        return self._keys_pressed.get(key, False)
 
    def is_key_released(self, key: int) -> bool:
        """
        Savoir si une touche est relâchée.
        :param key: L'identifiant de la touche.
        :return: Vrai si la touche est relâchée.
        """
        return self._keys_released.get(key, False)
    
    def is_key_down(self, key: int) -> bool:
        """
        Savoir si une touche est enfoncée.
        :param key: L'identifiant de la touche.
        :return: Vrai si la touche est enfoncée.
        """
        return self._keys_down.get(key, False)
    
    def is_button_pressed(self, button: int) -> bool:
        """
        Savoir si un bouton est pressée.
        :param key: L'identifiant du bouton.
        :return: Vrai si le bouton est pressée.
        """
        return self._buttons_pressed.get(button, False)

    def is_button_released(self, button: int) -> bool:
        """
        Savoir si un bouton est relâchée.
        :param key: L'identifiant du bouton.
        :return: Vrai si le bouton est relâchée.
        """
        return self._buttons_released.get(button, False)
    
    def is_button_down(self, button: int) -> bool:
        """
        Savoir si un bouton est enfoncée.
        :param key: L'identifiant du bouton.
        :return: Vrai si le bouton est enfoncée.
        """
        return self._buttons_down.get(button, False)

    def update_events(self) -> None:
        """
        Actualiser les évènements avec les nouvelles entrées arrivées.
        """

        # Retirer les évènements précédents
        self._keys_pressed: dict[int, bool] = {}
        self._keys_released: dict[int, bool]   = {}

        self._buttons_pressed: dict[int, bool] = {}
        self._buttons_released: dict[int, bool]   = {}

        self.alpha_numeric = str()
        self.quit: bool = False
        
        # Calcul de la position de la souris
        mouse_pos = pygame.Vector2(pygame.mouse.get_pos())
        self.delta_mouse_pos = mouse_pos - self.mouse_pos
        self.mouse_pos = mouse_pos

        # Actualise les évènements
        for event in pygame.event.get():
            # Détection de touches
            if event.type == pygame.KEYDOWN:
                self._keys_pressed[event.key] = True
                self._keys_down[event.key] = True

                # Essayer de détecter l'alpha numérique de l'entrée
                if event.unicode.isalnum():
                    self.alpha_numeric = event.unicode

            elif event.type == pygame.KEYUP:
                self._keys_released[event.key] = True
                self._keys_down[event.key] = False

            # Détection de boutons
            elif event.type == pygame.MOUSEBUTTONDOWN:
                self._buttons_pressed[event.button] = True
                self._buttons_down[event.button] = True
            elif event.type == pygame.MOUSEBUTTONUP:
                self._buttons_released[event.button] = True
                self._buttons_down[event.button] = False

            # Demande de fermeture de la fenêtre
            elif event.type == pygame.QUIT:
                self.quit = True
