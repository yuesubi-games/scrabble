from ..dictionary import Word
from ..letter import Letter


class FormedWord:
    """Un mot formé sur le plateau du Scrapple"""

    def __init__(self) -> None:
        """
        Constructeur d'un mot formé sur le plateau. [!] Les attributs doivent
        êtres complétés à la main !
        """
        self.letters: list[Letter] = list()
        self._is_vertical: bool = False  # TODO: supprimé si non utilisé
    
    def compute_word(self) -> Word:
        """
        Trouver le mot formé par les lettres.
        :return: Le mot formé par les lettres.
        """
        # Trier les lettres dans l'ordre
        in_order_letters = sorted(self.letters,
            key=lambda l: abs(l.pos_x) + abs(l.pos_y))
        
        # Former la chaîne de caractère du mot
        word_str = str()
        for letter in in_order_letters:
            word_str += letter.character
        
        return Word(word_str)