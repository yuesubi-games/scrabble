# Preuves de terminaison,  de correction, et de complexité


## Définitions

Dans les preuves suivantes on considère les notations suivantes:
* $h_X \in \N$, la hauteur de $X$, avec $X$ un ABR
* $R_X$, la racine de $X$, avec $X$ un ABR
* $e_N$, l'étiquette de $N$, avec $N$ un noeud
* $G_X$, l'arbre gauche de $X$, avec $X$ un ABR ou un noeud
* $D_X$, l'arbre droit de $X$, avec $X$ un ABR ou un noeud


## Recherche dans un ABR

### Algorithme
```txt
 1| fonction valeur_existe(A, e)
 2|     existe <- Faux
 3|     si e_R = e alors
 4|         existe <- Vrai
 5|     sinon si e < e_R alors
 6|         si gauche_existe( A ) alors
 7|             existe <- valeur_existe( gauche( A ), e )
 8|         fin si
 9|     sinon
10|         si droit_existe( A ) alors
11|             existe <- valeur_existe( droit( A ), e )
12|         fin si
13|     fin si
14|     retourne existe
15| fin fonction
```

### Preuve de terminaison
On cherche à prouver que l'algorithme de recherche se termine.

Pour chaque appel de la fonction sur un ABR $A$, on considère deux cas:
* Si $h_A = 0$ alors $G_A = \empty$ et $D_A = \empty$ donc l'algorithme de
    termine.
* Si $h_A \gt 0$ alors il existe au moins un sous arbre non vide. On considère
    encore deux cas:
    * Si $e = e_{R_A}$ alors l'algorithme se termine.
    * Sinon deux autres cas sont possibles:
        * Si la fonction n'est pas appelée sur un de ses sous arbres l'
            algorithme se termine.
        * Si la fonction est appelée sur un de ses sous arbres alors elle est
            appelée sur un arbre de hauteur $h_A - 1$.

A chaque appel de la fonction, soit l'algorithme se termine, soit il s'exécute
sur un arbre de hauteur $h_A - 1$, et ainsi la hauteur des arbres sur lesquels
la fonction est appelée décrois et converge vers $0$, car un ABR est une
structure finie. Or, l'algorithme termine pour $h_A = 0$. Donc l'algorithme
termine dans tous les cas.

### Calcul de complexité
On cherche à calculer la complexité de l'algorithme de recherche.

Si la fonction ne se rappelle pas elle-même alors la complexité de la fonction
est de l'ordre de $O(1)$.

L'algorithme part de $R_A$ et descent d'un niveau dans $A$ à chaque appel de la
fonction, pour atteindre un noeud final, ce qui forme une branche, notée $b$.
Ainsi la fonction est exécuté une fois par noeud de $b$. Or, pour calculer la
complexité, on considère le pire cas, et on sait que dans un ABR de hauteur $h$,
la branche la plus longue est de longueur $h$. Ainsi dans le pire cas la
fonction est exécutée $h_A$ fois et la complexité de la recherche est donc de
$O(h)$.

### Preuve de correction
On cherche à prouver que l'algorithme de recherche donne un résultat correct.

On considère deux cas:
* Si $h_A = 0$ alors la seule étiquette de $A$ est $e_{R_A}$. Donc il suffit de
    vérifier si $e_{R_A} = e$ pour savoir si l'étiquette existe dans $A$.
* Sinon il faut considérer trois cas:
    * Si $e_{R_A} = e$ alors l'étiquette existe dans $A$.
    * Sinon si $e \lt e_{R_A}$ alors, si $e$ existe dans l'arbre, elle est
        forcément dans $G_A$, si $G_A \ne \empty$, car toute les valeurs dans
        $G_A$ sont plus petites que $e_{R_A}$, ce qui est le cas de $e$. On
        appelle donc la fonction pour rechercher $e$ sur $G_A$.
    * Sinon on a forcément $e \gt e_{R_A}$, et si $e$ existe dans l'arbre, il
        est forcément dans $D_A$, si $D_A \ne \empty$, car toute les valeurs
        dans $D_A$ sont plus grandes que $e_{R_A}$, ce qui est le cas de $e$. On
        appelle donc la fonction pour rechercher $e$ sur $D_A$.

Tout les cas produits sont corrects, on peut donc dire que l'algorithme est
correct.


## Ajout dans un ABR

## Algorithme
```txt
 1| fonction ajout_valeur(A, e)
 2|     N <- A
 3|     tant que N différent de NULL faire
 4|         si e égal à etiquette( N ) alors
 5|             N <- NULL
 6|         sinon si e inférieur à etiquette( N ) alors
 7|             si gauche( N ) est NULL alors
 8|                 mettre_gauche( N , ABR( e ) )
 9|                 N <- NULL
10|             sinon
11|                 N <- gauche( N )
12|             fin si
13|         sinon
14|             si droit( N ) is NULL alors
15|                 mettre_droit( N, ABR( e ) )
16|                 N <- NULL
17|             sinon
21|                 N <- droit( N )
22|             fin si
23|         fin si
24|     fin tant que
25| fin fonction
```

## Preuve de terminaison
On cherche à prouver que l'algorithme d'ajout se termine.

Quand $N$ est attribué à $\empty$, alors l'algorithme se termine car la boucle
ne continue que si $N = \empty$ (`l3`), et $N$ est attribué qu'une fois par
itération de la boucle. Ainsi attribuer $N$ à $\empty$ est équivalent à l'
algorithme se termine.

On considère ainsi deux cas:
* Si $h_N = 0$ alors $G_N = \empty$ et $D_N = \empty$, donc $N$ est attribué à
    $\empty$, et l'algorithme se termine.
* Si $h_N \gt 0$ alors il existe au moins un sous arbre. On considère encore
    deux cas:
    * Si $e = e_{N}$ alors $N$ est attribué à $\empty$ (`l5`), et l'
        algorithme se termine.
    * Si $e \ne e_{N}$ alors deux autres cas sont possibles:
        * Si $N$ est attribué à $\empty$ (`l9` ou `l16`) alors l'algorithme se
            termine.
        * Si $N$ n'est pas attribué à $\empty$ alors la ligne `l11` ou `l21`
            est exécuté, et $N$ devient un arbre de hauteur $h_N - 1$, car un
            sous arbre à une hauteur qui est plus petite de $1$ comparé à son
            parent.

A chaque appel de la fonction, soit l'algorithme se termine, soit $H_N$ est
réduit de $1$, ainsi $H_N$ décrois et converge vers $0$, car un ABR est une
structure finie. Or, l'algorithme termine pour $h = 0$.

Donc l'algorithme termine dans tous les cas.

## Calcul de complexité
On cherche à calculer la complexité de l'algorithme d'ajout.

Le contenu de la boucle `tant que` est de l'ordre de $O(1)$. Ainsi la complexité
de l'algorithme est de $O(i)$, avec $i \in \N$ qui est le nombre d'itération de
la boucle `tant que`. On cherche donc à trouver la valeur de $i$.

Dans l'algorithme, $N$ est d'abord attribué à $R$, et est à chaque itération,
$N$ devient un sous arbre de lui-même, jusqu'à être attribué à $\empty$. On note
$N$ la dernière valeur prise par $N$ avant d'être $\empty$. Le chemin $R-N$ est
une branche, notée $b$, de 'ABR. La boucle `tant que` fait donc
$\text{Longueur}(b) + 1$ itération car $N$ prends comme valeur chaque noeud de
$b$ successivement, plus une fois la valeur $\empty$. Ainsi
$i = \text{Longueur}(b) + 1$.

Or, pour calculer la complexité, on considère le pire cas, et on sait que dans
un ABR de hauteur $h$, la branche la plus longue est de longueur $h$. Ainsi dans
le pire cas, $i = h + 1$, et la complexité de l'algorithme est donc de l'ordre
de $O(h)$.

## Preuve de correction
On cherche à prouver que l'algorithme d'ajout donne un résultat correct.

A chaque itération de la boucle `tant que`, on considère trois cas:
* Si $e = e_{R_{N}}$ alors il n'y a pas besoin d'ajouter l'étiquette dans $N$,
    car elle est déjà dedans. $e$ est bien dans $N$ et $N$ est reste valide, l'
    opération est donc correcte.
* Sinon si $e \lt e_{R_{N}}$ alors il faut que $e$ soit dans $G_N$, pour que $N$
    contienne $e$ et $N$ reste un ABR valide, on considère encore deux cas:
    * Si $G_N = \empty$, on attribue $G_N$ à un ABR, dont l'étiquette est $e$.
        Ainsi $e$ est bien dans $N$ et $N$ reste valide, l'opération est donc
        correcte.
    * Sinon on attribut $N$ à $G_N$, pour que $e$ soit inséré lors de la
        prochaine itération. Ainsi si la prochaine itération est correcte alors
        celle ci l'est aussi.
* Sinon on a $e \gt e_{R_{N}}$, et il faut que $e$ soit dans $D_N$, pour que $N$
    contienne $e$ et que $N$ reste un ABR valide, on considère encore deux cas:
    * Si $D_N = \empty$, on attribue $D_N$ à un ABR, dont l'étiquette est $e$.
        Ainsi $e$ est bien dans $N$ et $N$ reste valide, l'opération est donc
        correcte.
    * Sinon on attribut $N$ à $D_N$, pour que $e$ soit inséré lors de la
        prochaine itération. Ainsi si la prochaine itération est correcte alors
        celle ci l'est aussi.

La dernière itération est toujours correcte. Et si la dernière est valide alors
celle d'avant aussi, et ainsi de suite. Toutes les itérations sont donc valides,
et on peut ainsi dire que l'algorithme complet est correct.
