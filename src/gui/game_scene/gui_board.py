import pygame

from assets import *
from scrapple import Board, Letter

from .gui_letter import render_letter_to


class GuiBoard:
    def __init__(self, board: Board, position: pygame.Vector2, unit: float
            ) -> None:
        """
        Constructeur.
        :param board: Le plateau de jeu.
        :param position: La position du plateau de jeu.
        :param unit: La taille d'une cellule.
        """
        self._scrapple_board: Board = board

        self.position: pygame.Vector2 = position
        self.unit: float = unit

        self.temporary_letters: list[Letter] = list()

    def clear_temporary_letters(self) -> None:
        """Retirer toutes les lettres temporaires du plateau de jeu."""
        self.temporary_letters.clear()
    
    def is_position_above(self, global_position: pygame.Vector2) -> bool:
        """
        Vérifier si la position est au dessus du plateau.
        :param global_position: La position à vérifier.
        :return: True si la position est au dessus du plateau, False sinon.
        """
        local_pos = (global_position - self.position) / GUI_UNIT
        return 0 <= local_pos.x < 15 and 0 <= local_pos.y < 15
     
    def peak_letter_at(self, global_position: pygame.Vector2) -> Letter | None:
        """
        Récupérer la lettre à la position donnée sans la retirer.
        :param global_position: La position à utiliser pour trouver la lettre.
        :return: La lettre récupérée ou None.
        """
        local_pos = (global_position - self.position) / GUI_UNIT
        letter_found = None

        # Vérifier si la position est sur le plateau
        if 0 <= local_pos.x < 15 and 0 <= local_pos.y < 15:
            # Trouver si il y a une lettre qui peut être retirée
            board_pos = (int(local_pos.x), int(local_pos.y))
            for letter in self.temporary_letters:
                if letter.pos == board_pos:
                    letter_found = letter
        
        return letter_found
    
    def pop_letter_at(self, global_position: pygame.Vector2) -> Letter | None:
        """
        Récupérer la lettre à la position donnée.
        :param global_position: La position à utiliser pour trouver la lettre.
        :return: La lettre récupérée ou None.
        """
        local_pos = (global_position - self.position) / GUI_UNIT
        letter_found = None

        # Vérifier si la position est sur le plateau
        if 0 <= local_pos.x < 15 and 0 <= local_pos.y < 15:
            # Trouver si il y a une lettre qui peut être retirée
            board_pos = (int(local_pos.x), int(local_pos.y))
            for letter in self.temporary_letters:
                if letter.pos == board_pos:
                    letter_found = letter
        
        if letter_found is not None:
            self.temporary_letters.remove(letter_found)
        
        return letter_found
    
    def try_push_letter(self, global_position: pygame.Vector2, letter: Letter
            ) -> bool:
        """
        Ajouter une lettre à la main du joueur.
        :param global_position: La position où placer la lettre.
        :param letter: La lettre à ajouter.
        :return: True si la lettre à pu être posée, False sinon.
        """
        local_pos = (global_position - self.position) / GUI_UNIT
        letter.pos = (int(local_pos.x), int(local_pos.y))

        pos_available = self._scrapple_board.letter_at(letter.pos) is None
        pos_available = (pos_available and self.temporary_letter_at(letter.pos)
            is None)
        pos_available = pos_available and (0 <= local_pos.x < 15 and
            0 <= local_pos.y < 15)
        
        if pos_available:
            self.temporary_letters.append(letter)
        
        return pos_available
    
    def temporary_letter_at(self, position: tuple[int, int]) -> Letter | None:
        """
        Récupérer la lettre temporaire qui est aux coordonnées spécifiées.
        :param location: Les coordonnées de la lettre.
        :return: La lettre trouvée ou None si il n'y a pas de lettres.
        """
        letter_found = None
        
        # Parcourir les lettres pour vérifier si une lettre est à cette position
        for letter in self.temporary_letters:
            if letter.pos == position:
                letter_found = letter
        
        return letter_found
    
    def render_to(self, target: pygame.Surface) -> None:
        """
        Dessiner le plateau du scrapple.
        :param target: La surface sur laquelle dessiner.
        """
        for y, row in enumerate(self._scrapple_board.tiles):
            for x, tile in enumerate(row):
                # Le rectangle de la cellule
                rect = pygame.Rect(
                    self.position.x + self.unit*x,
                    self.position.y + self.unit*y,
                    self.unit,
                    self.unit
                )

                # Dessiner le fond de la cellule
                pygame.draw.rect(target,
                    BOARD_COLOR[(tile.letter_coef, tile.word_coef)], rect)
                # Dessiner le contour de la cellule
                pygame.draw.rect(target, BOARD_BORDER_COLOR, rect, 1)

                letter = self._scrapple_board.letter_at((x, y))
                if letter is None:
                    letter = self.temporary_letter_at((x, y))
                
                if letter is not None:
                    render_letter_to(target, letter,
                        pygame.Vector2(rect.center))