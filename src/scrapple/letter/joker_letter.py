from .letter import Letter


class JokerLetter(Letter):
    """
    Une lettre joker du scrapple, qui peut remplacer n'importe quelle lettre.
    """
    
    def __init__(self) -> None:
        """Constructeur d'une lettre joker."""
        super().__init__()
        self._chosen_character: str = 'A'
        self.has_chose_character: bool = False
    
    @property
    def character(self) -> str:
        return self._chosen_character
    
    @property
    def points(self) -> int:
        return 0
    
    def __repr__(self) -> str:
        return "JokerLetter"

    def turn_in_character(self, character: str) -> None:
        """
        Changer le caractère de la lettre.
        :param character: Le nouveau caractère que la lettre joker doit
            prendre.
        """
        self._chosen_character = character
        self.has_chose_character = True
    
    def turn_blank(self) -> None:
        """Remettre la lettre joker à blanc."""
        self.has_chose_character = False