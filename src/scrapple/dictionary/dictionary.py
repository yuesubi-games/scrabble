from assets import FRENCH_DICTIONARY_FILE
from data_structures import BinarySearchTree

from .word import Word


class Dictionary:
    """Un dictionnaire avec tout les mots qui sont valides pour le Scrapple."""

    @classmethod
    def french(cls) -> 'Dictionary':
        """
        Créer un dictionnaire français à partir d'un fichier texte.
        :return: Le dictionnaire créé.
        """
        # Liste de mots à mettre dans le dictionnaire
        word_list = list()

        with open(FRENCH_DICTIONARY_FILE, 'r') as dict_file:
            # Pour chaque ligne du fichier texte
            for line in dict_file.readlines():
                # Retirer les caractère inutiles
                proper_text = line.strip(" \t\n\r")
                # Ajouter le mot à la liste de mots
                word_list.append(Word(proper_text))
        
        # Créer un dictionnaire avec les mots récupérés
        dictionary = cls()
        dictionary.set_words_to(word_list)

        return dictionary
    
    def __init__(self) -> None:
        """Constructeur d'un dictionnaire vide."""
        self._world_bst: BinarySearchTree[Word] = BinarySearchTree(Word("none"))
    
    def set_words_to(self, words: list[Word]) -> None:
        """
        Remplacer les mots du dictionnaire.
        :param words: Les nouvelles mots à mettre dans le dictionnaire.
        """
        self._world_bst.set_values_to(words)
    
    def is_word_valid(self, word: Word) -> bool:
        """
        Vérifier si un mot existe dans le dictionnaire.
        :param word: Le mot à chercher dans le dictionnaire.
        :return: True si le mot est dans le dictionnaire et False sinon.
        """
        return self._world_bst.contains_value(word)
    
    def add_word(self, word: Word) -> None:
        """
        Ajouter un mot dans le dictionnaire.
        :param word: Le mot à ajouter.
        """
        self._world_bst.add_value(word)
        self._world_bst.balance()