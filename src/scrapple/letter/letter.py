import abc


class Letter(metaclass=abc.ABCMeta):
    """
    Une lettre du scrapple. Dériver de cette classe pour implémenter différent
    types de lettres.
    """
    
    def __init__(self) -> None:
        """Constructeur de la classe lettre."""
        self.pos: tuple[int, int] = (0, 0)

    @property
    def pos_x(self) -> int:
        """Assesseur de la coordonnée x de la lettre."""
        return self.pos[0]
    
    @property
    def pos_y(self) -> int:
        """Assesseur de la coordonnée y de la lettre."""
        return self.pos[1]

    @property
    @abc.abstractmethod
    def character(self) -> str:
        """
        Assesseur du caractère de la lettre.
        :return: Le caractère de la lettre.
        """
        raise NotImplementedError
    
    @property
    @abc.abstractmethod
    def points(self) -> int:
        """
        Assesseur des points que vaut la lettre.
        :return: Les points que vaut la lettre.
        """
        raise NotImplementedError