from ..dictionary import Word
from ..letter import Letter
from ..player import Player

from .action import Action


class PlaceWordAction(Action):
    """L'action de placer un mot sur le plateau du Scrapple."""

    def __init__(self, player: Player) -> None:
        """
        Constructeur de l'action. [!] Les attributs doivent êtres complétés à la
        main !
        :param player: Le joueur à l'origine de l'action.
        """
        super().__init__(player)
        self.words_placed: list[Word] = list()
        self.letters_used: list[Letter] = list()
        self.points_won: int = 0