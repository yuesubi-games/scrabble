import time
import threading

from scrapple import Dictionary


# Temps d'attente avant le lancement du chargement du cache. Il faut un temps d'
# attente pour que la fenêtre est le temps de s'ouvrir, car une fois que le
# chargement du cache est lancé, son fil d'exécution s'exécute en priorité par
# rapport au fil d'exécution principal.
_LOAD_START_DELAY: float = 1.0


class FrenchDictionary:
    """
    Classe statique avec les éléments pour acceder au dictionnaire en cache.
    """
    
    # Dictionnaire en cache
    _cached_dictionary: Dictionary | None = None
    # Verrou du dictionnaire
    _cached_dictionary_lock: threading.Lock = threading.Lock()

    @classmethod
    def _load_cache(cls) -> None:
        """Méthode de classe pour charger le dictionnaire en cache."""
        cls._cached_dictionary_lock.acquire()
        time.sleep(_LOAD_START_DELAY)
        cls._cached_dictionary = Dictionary.french()
        cls._cached_dictionary_lock.release()
    
    @classmethod
    def acquire(cls) -> Dictionary:
        """
        Méthode de classe pour acquérir le dictionnaire français qui est en
        cache. (!) La méthode de classe release doit être appelée pour libérer
        le dictionnaire quand il n'est plus utilisé.
        :return: Le dictionnaire français en cache.
        """
        cls._cached_dictionary_lock.acquire()
        if cls._cached_dictionary is None:
            raise BaseException("Dictionary not loaded !")
        return cls._cached_dictionary
    
    @classmethod
    def release(cls) -> None:
        """Méthode de classe appelée pour libérer le dictionnaire."""
        cls._cached_dictionary_lock.release()


# Lancer le chargement du dictionnaire dans un autre thread
_dictionary_load_thread = threading.Thread(target=FrenchDictionary._load_cache)
_dictionary_load_thread.start()