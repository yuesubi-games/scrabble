import pygame

from data_structures import BinarySearchTree
from scrapple import Word, Dictionary

from ..event_manager import EventManager
from ..scene_manager import Scene, SceneId, SceneManager
from ..widgets import Anchor, Text, TextButton, TextEntry, Fit, Frame

from .gui_word_bst import GuiWordBST


class BSTScene(Scene):
    """Le de l'ABR de l'application."""

    def __init__(self) -> None:
        super().__init__()

        self.middle_position: pygame.Vector2 = pygame.Vector2(0, 0)
        self.position: pygame.Vector2 = pygame.Vector2(0, -150)
        self.movement: pygame.Vector2 = pygame.Vector2(0, 0)

        # Dragging variables
        self.is_dragging: bool = False
        self.drag_start_mouse_pos: pygame.Vector2 = pygame.Vector2(0, 0)
        self.drag_start_pos: pygame.Vector2 = pygame.Vector2(0, 0)
        self.drag_position: pygame.Vector2 = pygame.Vector2(self.position)

        # bst: BinarySearchTree[Word] = Dictionary.french()._world_bst
        bst: BinarySearchTree[Word] = BinarySearchTree(Word("NONE"))
        bst.set_values_to([Word(chr(i + ord('A'))) for i in range(20)])
        self.gui_bst: GuiWordBST = GuiWordBST(bst, pygame.Vector2(0, 0))

        # Informations sur l'arbre

        self.height_text: Text = Text(
            pygame.Vector2(5, 5), Anchor.NW,
            text="Hauteur : 0",
            font_size=24,
            font_color=pygame.Color(200, 200, 200)
        )
        self.size_text: Text = Text(
            pygame.Vector2(5, 35), Anchor.NW,
            text="Taille : 0",
            font_size=24,
            font_color=pygame.Color(200, 200, 200)
        )

        # Bouton pour quitter

        self.quit_button = TextButton(
            pygame.Vector2(-5, 5), Anchor.NE,
            pygame.Vector2(100, 30), Fit.NONE,
            "Quitter",
            font_color=pygame.Color(0, 0, 0),
            font_size=26,
            background_color=pygame.Color(100, 100, 100),
            border_color=pygame.Color(100, 100, 100),
            border_width=2,
            command=self.quit_button_cmd
        )
        
        # Cadre avec les champs pour ajouter et chercher des éléments

        self.add_entry = TextEntry(
            pygame.Vector2(0, 0), Anchor.NW,
            pygame.Vector2(150, 30), Fit.NONE,
            font_color=pygame.Color(200, 200, 200),
            font_size=26,
            background_color=pygame.Color(100, 100, 100),
            border_color=pygame.Color(100, 100, 100),
            border_width=2,
        )
        self.search_entry = TextEntry(
            pygame.Vector2(0, 30 + 5), Anchor.NW,
            pygame.Vector2(150, 30), Fit.NONE,
            font_color=pygame.Color(200, 200, 200),
            font_size=26,
            background_color=pygame.Color(100, 100, 100),
            border_color=pygame.Color(100, 100, 100),
            border_width=2,
        )
        
        self.entry_frame: Frame = Frame(
            pygame.Vector2(5, -5), Anchor.SW,
            pygame.Vector2(200, 2*30 + 5), Fit.NONE,
            children=[
                self.add_entry,
                self.search_entry,
                TextButton(
                    pygame.Vector2(155, 0), Anchor.NW,
                    pygame.Vector2(100, 30), Fit.NONE,
                    "Ajouter",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.add_in_bst_button_cmd
                ),
                TextButton(
                    pygame.Vector2(155, 30 + 5), Anchor.NW,
                    pygame.Vector2(100, 30), Fit.NONE,
                    "Chercher",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.search_in_bst_button_cmd
                )
            ]
        )
         
        # Cadre avec les boutons de parcours

        self.walk_button_frame: Frame = Frame(
            pygame.Vector2(-5, -5), Anchor.SE,
            pygame.Vector2(200, 30*4 + 3*5), Fit.NONE,
            children=[
                TextButton(
                    pygame.Vector2(0, 0), Anchor.NW,
                    pygame.Vector2(200, 30), Fit.NONE,
                    "Parcours préfixe",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.pre_order_walk_button_cmd
                ),
                TextButton(
                    pygame.Vector2(0, 35*1), Anchor.NW,
                    pygame.Vector2(200, 30), Fit.NONE,
                    "Parcours infixe",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.in_order_walk_button_cmd
                ),
                TextButton(
                    pygame.Vector2(0, 35*2), Anchor.NW,
                    pygame.Vector2(200, 30), Fit.NONE,
                    "Parcours suffixe",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.post_order_walk_button_cmd
                ),
                TextButton(
                    pygame.Vector2(0, 35*3), Anchor.NW,
                    pygame.Vector2(200, 30), Fit.NONE,
                    "Parcours en largeur",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.bfs_button_cmd
                )
            ]
        )

        # Cadre principal

        self.frame: Frame = Frame(
            pygame.Vector2(0, 0), Anchor.NW,
            pygame.Vector2(1, 1), Fit.NONE,
            children=[
                self.height_text,
                self.size_text,
                self.quit_button,
                self.entry_frame,
                self.walk_button_frame
            ]
        )

        # Menu de notifications

        self.is_notification_displayed: bool = False

        self.notification_text: Text = Text(
            pygame.Vector2(0, -15), Anchor.C,
            text="[Insérez texte ici]",
            font_size=24,
            font_color=pygame.Color(200, 200, 200)
        )

        self.notification_menu: Frame = Frame(
            pygame.Vector2(0, 0), Anchor.C,
            pygame.Vector2(275, 100), Fit.NONE,
            background_color=pygame.Color(50, 50, 50),
            border_color=pygame.Color(75, 75, 75),
            border_width=2,
            children=[
                self.notification_text,
                # Bouton ok
                TextButton(
                    pygame.Vector2(0, 15), Anchor.C,
                    pygame.Vector2(50, 30), Fit.NONE,
                    "Ok",
                    font_color=pygame.Color(200, 200, 200),
                    font_size=24,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.notification_ok_button_cmd
                )
            ]
        )
    
    def register_events(self, event_manager: EventManager) -> None:
        self.frame.size = pygame.Vector2(pygame.display.get_window_size())
        self.frame.process_events(event_manager)

        self.movement = pygame.Vector2(0, 0)
        
        if event_manager.is_key_down(pygame.K_UP):
            self.movement.y += 1
        if event_manager.is_key_down(pygame.K_DOWN):
            self.movement.y -= 1
        if event_manager.is_key_down(pygame.K_RIGHT):
            self.movement.x -= 1
        if event_manager.is_key_down(pygame.K_LEFT):
            self.movement.x += 1
        
        if self.movement.length_squared() != 0.0:
            self.movement.normalize_ip()
        
        if self.is_dragging:
            self.drag_position = (self.drag_start_pos +
                (event_manager.mouse_pos - self.drag_start_mouse_pos))
        
        if event_manager.is_button_pressed(pygame.BUTTON_LEFT):
            self.is_dragging = True
            self.drag_start_mouse_pos = event_manager.mouse_pos
            self.drag_start_pos = self.position
        elif event_manager.is_button_released(pygame.BUTTON_LEFT):
            self.is_dragging = False

    def update_by(self, delta_time: float) -> None:
        if self.is_dragging:
            self.position = self.drag_position
        else:
            self.position += self.movement * 50 * delta_time
        
        self.gui_bst.position = self.position + self.middle_position
        self.frame.update(delta_time)

        self.height_text.change_text(f"Hauteur : {self.gui_bst.bst.height()}")
        self.size_text.change_text(f"Taille : {self.gui_bst.bst.width()}")

    def render_to(self, target_surface: pygame.Surface) -> None:
        self.middle_position = pygame.Vector2(target_surface.get_size()) / 2
        
        self.gui_bst.render_to(target_surface)
        
        self.frame.render(target_surface)
    
    # Notifications

    def display_notification(self, message: str) -> None:
        """
        Afficher une notification.
        :param message: Le message à afficher. 
        """
        if not self.is_notification_displayed:
            self.frame.add_child(self.notification_menu)
        self.notification_text.change_text(message)
        self.notification_menu.size.x = self.notification_text.size.x + 40
        self.is_notification_displayed = True

    def notification_ok_button_cmd(self) -> None:
        """Commande du bouton ok des notifications."""
        if self.is_notification_displayed:
            self.frame.remove_child(self.notification_menu)
        self.is_notification_displayed = False
    
    # Fonctions appelées par les boutons

    def quit_button_cmd(self) -> None:
        """Commande du bouton pour quitter la scène."""
        SceneManager.switch_scene(SceneId.MENU)
    
    def add_in_bst_button_cmd(self) -> None:
        """Commande du bouton pour ajouter un élément dans l'arbre."""
        to_add = self.add_entry.get_text()
        try:
            self.gui_bst.bst.add_value(Word(to_add))
        except ValueError as _:
            self.display_notification("Le mot est invalide !")
        self.add_entry.set_text("")
    
    def search_in_bst_button_cmd(self) -> None:
        """Commande du bouton pour chercher un élément dans l'arbre."""
        to_search = self.search_entry.get_text()
        try:
            word_to_search = Word(to_search)
            is_in_bst = self.gui_bst.bst.contains_value(word_to_search)
            message = ("Le mot est dans l'arbre" if is_in_bst else
                "Le mot n'est pas dans l'arbre")
            self.display_notification(message)
        except ValueError as _:
            self.display_notification("Le mot est invalide !")
        self.search_entry.set_text("")
    
    def pre_order_walk_button_cmd(self) -> None:
        """Commande du bouton parcours préfixe."""
        self.display_notification(";".join(
            word.word for word in self.gui_bst.bst.pre_order_walk()
        ))

    def in_order_walk_button_cmd(self) -> None:
        """Commande du bouton parcours infixe."""
        self.display_notification(";".join(
            word.word for word in self.gui_bst.bst.in_order_walk()
        ))

    def post_order_walk_button_cmd(self) -> None:
        """Commande du bouton parcours postfixe."""
        self.display_notification(";".join(
            word.word for word in self.gui_bst.bst.post_order_walk()
        ))
        
    def bfs_button_cmd(self) -> None:
        """Commande du bouton parcours en largeur."""
        self.display_notification(";".join(
            word.word for word in self.gui_bst.bst.bfs()
        ))