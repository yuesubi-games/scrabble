import typing

from .fifo import Queue


T = typing.TypeVar("T")


class BinarySearchTree(typing.Generic[T]):
    """
    Classe qui représente un ABR.
    [!] Les valeurs contenues dans l'ABR doivent pouvoir être comparables entre
    elles. 
    """
    
    @classmethod
    def from_ordered_values(ordered_values: list[T]) -> 'BinarySearchTree[T]':
        """
        Créer un ABR à partir des valeurs. Les valeurs sont insérés pour que la
        recherche puisse se faire en O(log2(n)) où n est la taille de l'ABR.
        :param ordered_values: Les nouvelles valeurs à mettre dans l'ABR, elles
            doivent être dans l'ordre croissant.
        :return: L'ABR créé.
        """
        bst = BinarySearchTree(None)
        bst.set_values_to_ordered(ordered_values)
        return bst
    
    @classmethod
    def from_values(ordered_values: list[T]) -> 'BinarySearchTree[T]':
        """
        Créer un ABR à partir des valeurs. Les valeurs sont insérés pour que la
        recherche puisse se faire en O(log2(n)) où n est la taille de l'ABR.
        :param ordered_values: Les nouvelles valeurs à mettre dans l'ABR.
        :return: L'ABR créé.
        """
        bst = BinarySearchTree(None)
        bst.set_values_to(ordered_values)
        return bst
    
    def __init__(
            self,
            value: T,
            left: 'BinarySearchTree[T] | None' = None,
            right: 'BinarySearchTree[T] | None' = None
        ) -> None:
        """
        Constructeur d'une ABR.
        :param left: L'ABR gauche.
        :param value: L'étiquette du noeud.
        :param right: L'ABR droite.
        """
        # Attribuer les valeurs
        self.left: BinarySearchTree[T] | None = left
        self.value: T = value
        self.right: BinarySearchTree[T] | None = right
    
    def __repr__(self) -> str:
        """
        Donne la représentation textuelle de l'ABR.
        :return: La représentation textuelle.
        """
        return self.textual_representation()
    
    @property
    def has_left(self) -> bool:
        """Retourne True si l'ABR à un arbre gauche, False sinon."""
        return self.left is not None
    
    @property
    def has_right(self) -> bool:
        """Retourne True si l'ABR à un arbre droit, False sinon."""
        return self.right is not None
     
    @property
    def is_node(self) -> bool:
        """Retourne True si l'ABR est un noeud, False sinon."""
        return self.has_left or self.has_right
    
    @property
    def is_leaf(self) -> bool:
        """Retourne True si l'ABR est une feuille, False sinon."""
        return (not self.has_left) and (not self.has_right)
    
    def textual_representation(self, depth: int = 0) -> str:
        """
        Donne la représentation textuelle de l'ABR.
        :param (optional) depth: La profondeur dans l'ABR, sert pour l'
            indentation.
        :return: La représentation textuelle.
        """
        # Texte final
        text = str()
        
        # Ajout du titre avant la représentation
        if depth == 0:
            text += "BinarySearchTree:\n"
        
        # Contiennent respectivement True si l'ABR droit et gauche existe et
        # False sinon.
        has_right = self.right is not None
        has_left = self.left is not None

        # Choisir le caractère à mettre après le noeud, pour représenter les
        # liens entre les différents noeuds.
        tail_char = str()
        if has_right and has_left:
            tail_char = '<'
        elif has_right:
            tail_char = '/'
        elif has_left:
            tail_char = '\\'

        # Si l'arbre droit existe, ajouter sa représentation avec une
        # indentation supplémentaire.
        if has_right:
            text += self.right.textual_representation(depth + 1)
        
        # Ajouter l'étiquette du noeud et le caractère pour représenter la
        # liaison.
        text += '\t' * depth + str(self.value) + ' ' + tail_char + '\n'

        # Si l'arbre gauche existe, ajouter sa représentation avec une
        # indentation supplémentaire.
        if has_left:
            text += self.left.textual_representation(depth + 1)
        
        return text
    
    def in_order_walk(self) -> list[T]:
        """
        Effectue le parcours infixe de l'ABR.
        :return: Une liste d'étiquettes des noeuds dans l'ordre dans lequel les
            noeuds sont découverts.
        """
        # Liste des valeurs
        values = list()
        
        # Parcours infixe de l'ABR de gauche
        if self.has_left:
            values.extend(self.left.in_order_walk())
            
        # Ajout de la valeur
        values.append(self.value)
        
        # Parcours infixe de l'ABR de droite
        if self.has_right:
            values.extend(self.right.in_order_walk())
        
        return values

    def pre_order_walk(self) -> list[T]:
        """
        Effectue le parcours préfixe de l'ABR.
        :return: Une liste d'étiquettes des noeuds dans l'ordre dans lequel les
            noeuds sont découverts.
        """
        # Liste des valeurs
        values = list()
        
        # Ajout de la valeur
        values.append(self.value)

        # Parcours préfixe de l'ABR de gauche
        if self.has_left:
            values.extend(self.left.pre_order_walk())
        
        # Parcours préfixe de l'ABR de droite
        if self.has_right:
            values.extend(self.right.pre_order_walk())
        
        return values

    def post_order_walk(self) -> list[T]:
        """
        Effectue le parcours suffixe de l'ABR.
        :return: Une liste d'étiquettes des noeuds dans l'ordre dans lequel les
            noeuds sont découverts.
        """
        # Liste des valeurs
        values = list()

        # Parcours suffixe de l'ABR de gauche
        if self.has_left:
            values.extend(self.left.post_order_walk())
        
        # Parcours suffixe de l'ABR de droite
        if self.has_right:
            values.extend(self.right.post_order_walk())
        
        # Ajout de la valeur
        values.append(self.value)
        
        return values
    
    def bfs(self) -> list[T]:
        """
        Effectue le parcours en largeur de l'ABR.
        :return: Une liste d'étiquettes des noeuds dans l'ordre dans lequel les
            noeuds sont découverts.
        """
        values = list()
        
        queue = Queue()
        queue.enqueue(self)
        
        while not queue.is_empty:
            node = queue.dequeue()
            values.append(node.value)
            
            if node.has_left:
                queue.enqueue(node.left)
            if node.has_right:
                queue.enqueue(node.right)

        return values

    def width(self) -> int:
        """
        Trouver la taille de l'ABR.
        :return: La taille de l'ABR.
        """
        # Trouver la taille de la partie gauche si elle existe
        left_size = self.left.width() if self.has_left else 0
        # Trouver la taille de la partie droite si elle existe
        right_size = self.right.width() if self.has_right else 0
        # Ajouter la taille du sous ABR droit et gauche + 1 pour le noeud
        # actuel
        return 1 + left_size + right_size
    
    def height(self) -> int:
        """
        Trouver la hauteur de l'ABR.
        :return: La hauteur de l'ABR.
        """
        # Trouver la hauteur du sous ABR gauche, et si il est nul -1
        left_height = self.left.height() if self.has_left else -1
        # Trouver la hauteur du sous ABR droit, et si il est nul -1
        right_height = self.right.height() if self.has_right else -1
        # La hauteur est la plus grande hauteur de sous ABR plus 1
        return 1 + max(left_height, right_height)
    
    def contains_value(self, value: T) -> bool:
        """
        Vérifier si l'étiquette est présente dans l'ABR.
        [!] Il faut que les étiquettes de l'ABR soient des chaînes de
        caractères convertibles en entier.
        :param value: L'étiquette à essayer de trouver.
        :return: True si l'étiquette est trouvée False sinon.
        """
        # Variable de retour, à False par défaut
        exists = False
        
        # L'étiquette de l'ABR est la même que celle recherchée
        if value == self.value:
            exists = True
        # L'étiquette de l'ABR est plus grande que celle recherchée 
        elif value < self.value:
            # Si un sous ABR existe rechercher l'étiquette dedans
            if self.has_left:
                exists = self.left.contains_value(value)
        # L'étiquette de l'ABR est plus petite que celle recherchée 
        else:
            # Si un sous ABR existe rechercher l'étiquette dedans
            if self.has_right:
                exists = self.right.contains_value(value)

        return exists
    
    def balance(self) -> None:
        """
        Équilibrer l'ABR pour que la recherche puisse se faire en O(log2(n)) où
        n est la taille de l'ABR.
        """
        self.set_values_to_ordered(self.in_order_walk())
    
    def set_values_to(self, new_values: list[T]) -> None:
        """
        Remplacer les valeurs de l'ABR. Les nouvelles valeurs sont insérés pour
        que la recherche puisse se faire en O(log2(n)) où n est la taille de l'
        ABR.
        :param new_values: Les nouvelles valeurs à mettre dans l'ABR.
        """
        self.set_values_to_ordered(list(sorted(new_values)))
    
    def set_values_to_ordered(self, ordered_values: list[T]) -> None:
        """
        Remplacer les valeurs de l'ABR. Les nouvelles valeurs sont insérés pour
        que la recherche puisse se faire en O(log2(n)) où n est la taille de l'
        ABR.
        :param ordered_values: Les nouvelles valeurs à mettre dans l'ABR, elles
            doivent être dans l'ordre croissant.
        """
        # Les ABR à traiter et le contenu qui doit être ajouté dedans.
        next_target_bst = [self]
        to_insert_next = [ordered_values]
        
        while len(next_target_bst) > 0:
            # Les prochains ABR et contenus à traiter deviennent ceux en cours
            # de traitement
            target_trees = next_target_bst
            to_insert = to_insert_next
            
            # Vider la liste des prochains ABR et contenus à traiter
            next_target_bst = list()
            to_insert_next = list()
            
            # Pour chaque ABR et les valeurs qui doivent être ajouter dedans
            for bst, values in zip(target_trees, to_insert):
                # Liste de valeurs
                # +----+----+ - - +----+ - - +----+
                # | v1 | v2 | - - | vm | - - | vn |
                # +----+----+ - - +----+ - - +----+
                #                  /|\
                # <-------------->  |   <--------->
                #  partie gauche    |  partie droite
                #            valeur du milieu

                # Le nombre de valeurs à ajouter
                values_amount = len(values)
                # Trouver le milieu de la liste de valeurs
                middle_index = values_amount // 2
                # La valeur du milieu devient l'étiquette de l'ABR
                bst.value = values[middle_index]
                
                if values_amount < 2:
                    # Pas de valeurs dans la partie gauche
                    bst.left = None
                else:
                    # Donner les valeurs de la partie gauche à traiter 
                    bst.left = BinarySearchTree(None)
                    next_target_bst.append(bst.left)
                    to_insert_next.append(values[:middle_index])
                
                if values_amount < 3:
                    # Pas de valeurs dans la partie droite
                    bst.right = None
                else:
                    # Donner les valeurs de la partie droite à traiter 
                    bst.right = BinarySearchTree(None)
                    next_target_bst.append(bst.right)
                    to_insert_next.append(values[middle_index + 1:])
    
    def add_value(self, value: T) -> None:
        """
        Ajouter un valeur dans l'ABR.
        :param value: La valeur à ajouter.
        """
        # L'ABR dans lequel la valeur doit être ajoutée
        bst_destination = self

        # Tant qu'il y a un ABR dans lequel la valeur doit être ajouté
        # continuer
        while bst_destination is not None:
            if value == bst_destination.value:
                # Pas besoin d'ajouter la valeur elle existe déjà
                bst_destination = None
            
            elif value < bst_destination.value:
                if bst_destination.left is None:
                    # Il n'y a pas d'ABR gauche, il faut donc en créer un pour
                    # pouvoir mettre la valeur dedans
                    bst_destination.left = BinarySearchTree(value=value)
                    bst_destination = None
                else:
                    # Il faut chercher dans l'ABR gauche
                    bst_destination = bst_destination.left
            
            else:
                if bst_destination.right is None:
                    # Il n'y a pas d'ABR droit, il faut donc en créer un pour
                    # pouvoir mettre la valeur dedans
                    bst_destination.right = BinarySearchTree(value=value)
                    bst_destination = None
                else:
                    # Il faut chercher dans l'ABR droit
                    bst_destination = bst_destination.right