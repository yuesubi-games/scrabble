import pygame

from scrapple import Player

from ..scene_manager import Scene, SceneId, SceneManager
from ..event_manager import EventManager
from ..widgets import Anchor, Text, TextButton, Fit, Frame


class GameScoreScene(Scene):
    """Scène pour afficher les scores en fin de jeu."""

    def __init__(self, scores: dict[Player, int]) -> None:
        """
        Constructeur.
        :param scores: Un dictionnaire qui associe à un joueur son score.
        """
        super().__init__()

        # Les scores
        self.scores: tuple[Player, int] = self._ranked_scores(scores)

        # Cadre principal
        self.frame: Frame = Frame(
            pygame.Vector2(0, 0), Anchor.NW,
            pygame.Vector2(1, 1), Fit.NONE,
            children=[
                Text(
                    pygame.Vector2(0, (i - 2)*45), Anchor.C,
                    f"#{i + 1} {player.name} {score} pts",
                    font_color=pygame.Color(200, 200, 200),
                    font_size=44,
                )
                for i, (player, score) in enumerate(self.scores)
            ] + [
                # Bouton pour commencer une nouvelle partie
                TextButton(
                    pygame.Vector2(80, 150), Anchor.C,
                    pygame.Vector2(100, 30), Fit.NONE,
                    "Jouer",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.play_button_cmd
                ),
                # Bouton aller au menu principal
                TextButton(
                    pygame.Vector2(-80, 150), Anchor.C,
                    pygame.Vector2(100, 30), Fit.NONE,
                    "Menu",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.menu_button_cmd
                )
            ]
        )

    ############################################################################
    # MÉTHODES DE LA SCÈNE

    def register_events(self, event_manager: EventManager) -> None:
        self.frame.process_events(event_manager)

    def update_by(self, delta_time: float) -> None:
        self.frame.size = pygame.Vector2(pygame.display.get_window_size())
        self.frame.update(delta_time)

    def render_to(self, target_surface: pygame.Surface) -> None:
        self.frame.render(target_surface)

    ############################################################################
    # MÉTHODES APPELÉES PAR LES BOUTONS

    def menu_button_cmd(self) -> None:
        """Commande du bouton pour aller au menu."""
        SceneManager.switch_scene(SceneId.MENU)

    def play_button_cmd(self) -> None:
        """Commande du bouton pour commencer une partie."""
        SceneManager.switch_scene(SceneId.GAME)

    ############################################################################
    # MÉTHODES UTILITAIRES
    
    @staticmethod
    def _ranked_scores(scores: dict[Player, int]) -> list[tuple[Player, int]]:
        """
        Ranger les joueurs dans l'ordre de décroissant du score.
        :param scores: Un dictionnaire qui associe à un joueur son score.
        :return: Une liste de tuple joueur score trié dans l'ordre décroissant
            du score. 
        """
        return sorted(
            [(player, score) for player, score in scores.items()],
            key=lambda player_score: player_score[1],
            reverse=True
        )