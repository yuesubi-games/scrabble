"""Module des actions du jeu."""

from .action import Action
from .place_word_action import PlaceWordAction
from .trash_letters_action import TrashLettersAction