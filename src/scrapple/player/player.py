from ..letter import Letter


class Player:
    """Un joueur du Scrapple."""

    def __init__(self, name: str) -> None:
        """
        Constructeur.
        :param name: Le nom du joueur.
        """

        self.name: str = name

        # Les lettres que le joueur à dans sa main
        self.deck: list[Letter] = list()