import random

from assets import AMOUNT_OF_LETTERS, JOKER_CHAR
from ..letter import *


class Bag:
    """Sac où les joueurs peuvent piocher des lettres."""

    def __init__(self, amount_of_letters: dict[str, int] = AMOUNT_OF_LETTERS
            ) -> None:
        """
        Constructeur du sac de pioche.
        :param (optional) amount_of_letters: Un dictionnaire où chaque lettre
            majuscule correspond au nombre de fois que la lettre apparaît.
        """

        self._letters: list[Letter] = list()
        self.add_letters_from(amount_of_letters)

    def add_letters_from(self, amount_of_letters: dict[str, int]) -> None:
        """
        Ajouter des lettres dans le sac.
        :param amount_of_letters: Un dictionnaire où chaque lettre majuscule (ou
            un espace, qui représente le joker) correspond au nombre de fois
            que la lettre apparaît.
        """
        for key, value in amount_of_letters.items():
            # Ajouter la lettre le nombre de fois qu'elle est disponible
            if key == JOKER_CHAR:
                # La lettre est le joker
                self._letters.extend([JokerLetter() for _ in range(value)])
            else:
                self._letters.extend([BasicLetter(key) for _ in range(value)])
        
        self.shuffle()
    
    def shuffle(self) -> None:
        """Mélanger les lettres du sac."""
        random.shuffle(self._letters)
    
    def letters_amount(self) -> int:
        """
        Donne le nombre de lettres du sac.
        :return: Le nombre de lettres.
        """
        return len(self._letters)

    def pick_n_letter(self, amount: int) -> list[Letter]:
        """
        Piocher n lettres du sac. Si il y en a moins que n alors toutes sont
        données.
        :param amount: La quantité de lettres à piocher.
        :return: Une liste de lettres qui ont été piochée.
        """
        amount_to_pick = min(amount, len(self._letters))
        return [self.pick_letter() for _ in range(amount_to_pick)]

    def pick_letter(self) -> Letter:
        """
        Piocher une lettre du sac.
        :return: La lettre qui à été piochée.
        """
        return self._letters.pop()
    
    def put_letter_back(self, letter: Letter) -> None:
        """
        Remettre une lettre dans le sac.
        :letter: La lettre à remettre dans le sac.
        """
        self._letters.append(letter)