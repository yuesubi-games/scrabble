import unittest

from scrapple import Dictionary, Word


class TestDictionary(unittest.TestCase):
    """Test du dictionnaire."""

    def test_is_word_valid(self) -> None:
        """Test de la méthode "test_is_word_valid"."""
        d = Dictionary.french()
        self.assertTrue(d.is_word_valid(Word("zoo")))
        self.assertTrue(d.is_word_valid(Word("pomme")))
        self.assertTrue(d.is_word_valid(Word("poire")))
        self.assertTrue(d.is_word_valid(Word("yeti")))
        self.assertTrue(d.is_word_valid(Word("pommes")))
        self.assertTrue(d.is_word_valid(Word("elephant")))
        self.assertFalse(d.is_word_valid(Word("furansu")))
        self.assertFalse(d.is_word_valid(Word("airplane")))
        self.assertFalse(d.is_word_valid(Word("arbeiten")))
        self.assertFalse(d.is_word_valid(Word("kaboom")))
        self.assertFalse(d.is_word_valid(Word("fiiuuuuu")))
        self.assertFalse(d.is_word_valid(Word("nsi")))