import unittest

from data_structures import Stack


class TestStack(unittest.TestCase):
    """Test de la structure de données Pile."""
    
    def test_push_pop(self) -> None:
        """Test des méthodes "pop" et "push"."""
        test_values: list[int] = [8, 5, 8, -9, 9, 6, 8, 4]
        s = Stack()
        
        for value in test_values:
            s.push(value)
        
        for value in reversed(test_values):
            self.assertEqual(s.pop(), value)
    
    def test_is_empty(self) -> None:
        """Test de l'accesseur "est_vide"."""
        self.assertTrue(Stack().is_empty)

        s = Stack()
        s._internal_list = [4, 5, 6]
        self.assertFalse(s.is_empty)
    
    def test_len(self) -> None:
        """Test de la méthode "__len__"."""
        s = Stack()
        
        s._internal_list = [i for i in range(9)]
        self.assertEqual(len(s), 9)
        s._internal_list = [i for i in range(2)]
        self.assertEqual(len(s), 2)
        s._internal_list = [i for i in range(5)]
        self.assertEqual(len(s), 5)

