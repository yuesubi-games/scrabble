from ..letter import Letter
from ..player import Player

from .action import Action


class TrashLettersAction(Action):
    """
    L'action de défausser des lettres. Aucune ou toutes peuvent êtres
    défaussées.
    """

    def __init__(self, player: Player) -> None:
        """
        Constructeur de l'action. [!] Les attributs doivent êtres complétés à la
        main !
        :param player: Le joueur à l'origine de l'action.
        """
        super().__init__(player)
        self.letters_trashed: list[Letter] = list()