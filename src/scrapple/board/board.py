import itertools

from assets import BOARD_LETTER_COEF, BOARD_WORD_COEF
from ..dictionary import Word
from ..exceptions import *
from ..letter import Letter
from .tile import Tile

from .formed_word import FormedWord


class Board:
    """Le plateau de jeu du Scrapple."""

    def __init__(self, board_letter_coef: list[list[int]] = BOARD_LETTER_COEF,
            board_word_coef: list[list[int]] = BOARD_WORD_COEF) -> None:
        """
        Constructeur d'un plateau.
        :param board_letter_coef: Un tableau 2D avec les coefficients de chaque
            lettre posée sur la cellule.
        :param board_word_coef: Un tableau 2D avec les coefficients de chaque
            mot posé sur la cellule.
        """
        self.tiles: list[list[Tile]] = list()
        self._letters: list[Letter] = list()

        # Remplir le plateau avec des cellules
        for letter_row, word_row in zip(board_letter_coef, board_word_coef):
            self.tiles.append([Tile(letter_coef, word_coef)
                for letter_coef, word_coef in zip(letter_row, word_row)])
    
    def letter_at(self, location: tuple[int, int]) -> Letter | None:
        """
        Récupérer la lettre qui est aux coordonnées spécifiées.
        :param location: Les coordonnées de la lettre.
        :return: La lettre trouvée ou None si il n'y a pas de lettres.
        """
        letter_found = None
        
        # Parcourir les lettres pour vérifier si une lettre est à cette position
        for letter in self._letters:
            if letter.pos == location:
                letter_found = letter
        
        return letter_found

    def letters_beside(self, location: tuple[int, int]) -> list[Letter]:
        """
        Trouver les lettres adjacentes aux coordonnées spécifiées.
        :param location: Les coordonnées autour de laquelle chercher.
        :return: La liste de lettres trouvées.
        """
        near_letters = list()
        
        for letter in self._letters:
            distance = abs(letter.pos_x - location[0])
            distance += abs(letter.pos_y - location[1])
            if distance == 1:
                near_letters.append(letter)
        
        return near_letters
    
    def compute_score(self, formed_words: list[FormedWord]) -> int:
        """
        Calculer le score formé par les mots.
        :param formed_words: Les mots formés sur le plateau.
        :return: Les points gagnés par les mots formés.
        """
        score = 0

        for word in formed_words:
            # Le score du mot et le multiplicateur de mot
            word_score = 0
            word_multiplier = 1

            # Pour chaque lettre
            for letter in word.letters:
                tile = self.tiles[letter.pos_y][letter.pos_x]
                # Si la cellule à déjà été utilisée ne pas ajouter les
                # multiplicateurs.
                if tile.used:
                    word_score += letter.points
                else:
                    word_score += letter.points * tile.letter_coef
                    word_multiplier *= tile.word_coef
            
            # Ajouter le score du mot au score total
            score += word_score * word_multiplier
        
        return score

    def place_letters(self, letters: list[Letter]) -> None:
        """
        Placer les lettres sur le plateau de jeu.
        :param letters: Les lettre à placer.
        """
        # Ajouter les lettres au plateau
        self._letters.extend(letters)

        # Désactiver les bonus des cellules
        for letter in letters:
            self.tiles[letter.pos_y][letter.pos_x].used = True

    def detect_formed_words_by_letters(self, letters: list[Letter]
            ) -> list[FormedWord]:
        """
        Détecter les mots qui sont formés par les lettres .
        :param letters: Les lettres à placer.
        :return: True si les lettres peuvent êtres placées, False sinon.
        """
        # Vérification que les paramètres sont au bon format
        if len(letters) < 1:
            raise ValueError("Il faut placer au moins une lettre !")
        
        # Vérifier que les lettres sont bien placées
        if not self._are_letters_aligned(letters):
            raise ScrappleLettersNotAlined()
        if self._are_letters_overlapping(letters):
            raise ScrappleLettersOverlapping()
        if not self._are_letters_connected(letters):
            raise ScrappleLettersNotConnected()
        if not self._is_word_connected(letters):
            raise ScrappleWordNotConnected()
        
        # TODO : vérifier si les mots sont connectés au autres

        # Détecter si le mot est vertical ou non
        is_vertical = letters[0].pos_x == letters[-1].pos_x

        # Ajouter le mot principal
        words = list()
        principal_word = self._detect_single_word(letters, vertical=is_vertical)

        # Si une seule lettre est posée ne pas produire d'erreurs
        if len(principal_word.letters) > 1:
            words.append(principal_word)

        # Trouver chaque mot en parallèle du mot principal
        for letter in letters:
            # Ajouter les mots formés en parallèle des lettres placées
            perpendicular = self._detect_single_word([letter],
                vertical=not is_vertical)
            if len(perpendicular.letters) > 1:
                words.append(perpendicular)
        
        return words
    
    def _detect_single_word(self, letters_to_use: list[Letter],
            vertical: bool) -> FormedWord: 
        """
        Détecter un mot sur le plateau.
        :param letters_to_use: Les lettres à partir des quelles détecter le
            reste du mot.
        :param vertical: True si le mot à détecter est vertical, False si il est
            horizontal.
        :return: Le mot formé par les lettres.
        """
        to_check = letters_to_use.copy()
        formed_word = FormedWord()

        while len(to_check) > 0:
            # Détecter les lettres adjacentes et les ajouter à la liste de
            # celles à chercher si elle ne pas déjà été fait
            checking = to_check.pop()
            nearby_letters = self._adjacent_letters_of(checking.pos,
                vertical=vertical)

            for letter in nearby_letters:
                if letter not in to_check and letter not in formed_word.letters:
                    to_check.append(letter)
            
            formed_word.letters.append(checking)

        return formed_word

    def _adjacent_letters_of(self, location: tuple[int, int], vertical: bool
            ) -> list[Letter]:
        """
        Trouver les lettres adjacentes aux coordonnées spécifiées.
        :param location: Les coordonnées autour de laquelle chercher.
        :param vertical: Si vertical est True alors les lettres verticales
            adjacentes sont détectés, sinon les lettres horizontales le seront.
        :return: La liste de lettres trouvées.
        """
        nearby_letters = list()
        
        for letter in self._letters:
            delta_x = letter.pos_x - location[0]
            delta_y = letter.pos_y - location[1]

            # Détecter si les lettres sont adjacentes
            if ((delta_x == 0 and abs(delta_y) == 1 and vertical) or
                (abs(delta_x) == 1 and delta_y == 0 and not vertical)):
                nearby_letters.append(letter)
        
        return nearby_letters
    
    def _are_letters_aligned(self, letters: list[Letter]) -> bool:
        """
        Vérifier si les lettres sont alignées.
        :param letters: Les lettres à vérifier.
        :return: True si les lettres sont alignées, False sinon.
        """

        # Variables de détections de lettres non alignées
        placed_horizontally = True
        placed_vertically = True
        sample_letter = letters[0]

        # Pour chaque lettre associée à sa localisation sur le plateau
        for letter in letters:
            placed_horizontally = (placed_horizontally and
                sample_letter.pos_x == letter.pos_x)
            placed_vertically = (placed_vertically and
                sample_letter.pos_y == letter.pos_y)
        
        # Vrai si les lettres sont alignées horizontalement ou verticalement
        return placed_horizontally or placed_vertically
    
    def _are_letters_overlapping(self, letters: list[Letter]) -> bool:
        """
        Vérifier si des lettres sont superposées.
        :param letters: Les lettres à vérifier.
        :return: True si au moins une des lettres est superposées, False sinon.
        """
        overlapping = False

        # Vérifier si une localisation est déjà utilisée sur le plateau
        for letter in letters:
            tile_used = self.tiles[letter.pos_y][letter.pos_x].used
            overlapping = overlapping or tile_used
        
        # Vérifier si deux lettres veulent êtres placées au même endroit
        for letter1, letter2 in itertools.combinations(letters, 2):
            overlapping = overlapping or letter1.pos == letter2.pos

        return overlapping
    
    def _are_letters_connected(self, letters: list[Letter]) -> bool:
        """
        Vérifier si les lettres sont connectées. [!] Les lettres doivent êtres
        alignées.
        :param letters: Les lettres.
        :return: True si les lettres sont connectées, False sinon.
        """
        if len(letters) < 1:
            raise ValueError("Il faut placer au moins une lettre !")
        
        in_order_letters = sorted(letters,
            key=lambda l: abs(l.pos_x) + abs(l.pos_y))

        # Point minimal et maximal
        min_loc = in_order_letters[0].pos
        max_loc = in_order_letters[-1].pos
        
        all_connected = True

        # Vérifier que toutes les lettres sont connectés. La double boucle
        # devient une boucle simple car les lettres sont soit en diagonales soit
        # à l'horizontal.
        for y in range(min_loc[1], max_loc[1]):
            for x in range(min_loc[0], max_loc[0]):
                if in_order_letters[0].pos == (x, y):
                    in_order_letters.pop(0)
                else:
                    all_connected = all_connected and self.tiles[y][x].used

        return all_connected
    
    def _is_word_connected(self, letters: list[Letter]) -> bool:
        """
        Vérifier si un mot est connectés aux autres déjà posées ou au
        centre (pour le premier tour).
        :param letters: Les lettres du mot qui doit être connecté au reste.
        :return: True si le mot est connecté, False sinon.
        """
        is_connected = False
        for letter in letters:
            is_connected = is_connected or letter.pos == (7, 7)
            is_connected = (is_connected or
                len(self.letters_beside(letter.pos)) > 0)
        return is_connected