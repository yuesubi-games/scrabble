"""Gestion des ressources du projet."""

from .board import *
from .dictionaries import FRENCH_DICTIONARY_FILE
from .gui import *
from .letters import *