import unittest

from data_structures import Queue


class TestQueue(unittest.TestCase):
    """
    Test de la structure de données File.
    """
    
    def test_enqueue_dequeue(self) -> None:
        """Test des méthodes "enqueue" et "dequeue"."""
        test_values = [False, 5, "e", "Hello, world!", 9, 6]
        q = Queue()
        
        for value in test_values:
            q.enqueue(value)
        
        for value in test_values:
            self.assertEqual(q.dequeue(), value)
    
    def test_is_empty(self) -> None:
        """Test de la méthode "is_empty"."""
        self.assertTrue(Queue().is_empty)

        q = Queue()
        q._internal_list = [4, 5, 6]
        self.assertFalse(q.is_empty)
    
    def test_len(self) -> None:
        """Test de la méthode "__len__"."""
        q = Queue()
        
        q._internal_list = [i for i in range(9)]
        self.assertEqual(len(q), 9)
        q._internal_list = [i for i in range(2)]
        self.assertEqual(len(q), 2)
        q._internal_list = [i for i in range(5)]
        self.assertEqual(len(q), 5)