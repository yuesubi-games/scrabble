import typing


T = typing.TypeVar("T")


class Queue(typing.Generic[T]):
    """Classe qui représente la structure FIFO, file."""

    def __init__(self) -> None:
        """
        Constructeur d'une file. La file est vide.
        """
        self._internal_list: list[T] = list()
    
    @property
    def is_empty(self) -> bool:
        """
        Vérifier si la file est vide.
        :return: Retourne True si la file est vide et False sinon.
        """
        return len(self._internal_list) == 0
    
    def enqueue(self, value: T) -> None:
        """
        Enfiler un élément dans la file.
        :param value: La valeur à enfiler.
        """
        self._internal_list.append(value)
    
    def dequeue(self) -> T:
        """
        Défiler un élément de la file.
        :return: La valeur défilée.
        """
        return self._internal_list.pop(0)
    
    def __len__(self) -> int:
        """
        Récupérer la taille de la file.
        :return:
        """
        return len(self._internal_list)