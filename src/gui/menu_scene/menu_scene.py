import pygame

from ..event_manager import EventManager
from ..scene_manager import Scene, SceneId, SceneManager
from ..widgets import Anchor, TextButton, Fit, Frame


class MenuScene(Scene):
    """Le menu principal de l'application."""

    def __init__(self) -> None:
        super().__init__()

        self.frame: Frame = Frame(
            pygame.Vector2(0, 0), Anchor.NW,
            pygame.Vector2(1, 1), Fit.NONE,
            children=[
                # Bouton de jeu
                TextButton(
                    pygame.Vector2(0, -50), Anchor.C,
                    pygame.Vector2(250, 50), Fit.NONE,
                    "Jouer",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.play_button_cmd
                ),
                # Bouton de la scène de visualisation de l'ABR
                TextButton(
                    pygame.Vector2(0, 50), Anchor.C,
                    pygame.Vector2(250, 50), Fit.NONE,
                    "Voir le dictionnaire",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.bst_button_cmd
                )
            ]
        )

    def play_button_cmd(self) -> None:
        SceneManager.switch_scene(SceneId.GAME)
    
    def bst_button_cmd(self) -> None:
        SceneManager.switch_scene(SceneId.BST)
    
    def register_events(self, event_manager: EventManager) -> None:
        self.frame.process_events(event_manager)
    
    def update_by(self, delta_time: float) -> None:
        self.frame.size = pygame.Vector2(pygame.display.get_window_size())
        self.frame.update(delta_time)

    def render_to(self, target_surface: pygame.Surface) -> None:
        self.frame.render(target_surface)