class Word:
    """Un mot du dictionnaire."""
    
    def __init__(self, word_text: str) -> None:
        """
        Constructeur d'un mot.
        :param word_text: Le mot en text. Doit être composé uniquement de
            lettres ascii.
        """
        # Vérification de si le texte ne contient bien que des lettres en ascii
        if not word_text.isalpha() or not word_text.isascii():
            raise ValueError("The text \"" + word_text +
                "\" doesn't contain only ascii letters !")
        
        # Conversion du mot en majuscules
        self.word: str = word_text.upper()
    
    def __eq__(self, other: 'Word') -> bool:
        """
        Opérateur égal du mot.
        :param other: L'autre mot avec lequel comparer.
        :return: True si les mots sont égaux False sinon.
        """
        return self.word == other.word
    
    def __ne__(self, other: 'Word') -> bool:
        """
        Opérateur non-égal du mot.
        :param other: L'autre mot avec lequel comparer.
        :return: True si les mots ne sont pas égaux False sinon.
        """
        return self.word != other.word
    
    def __lt__(self, other: 'Word') -> bool:
        """
        Opérateur plus petit que du mot.
        :param other: L'autre mot avec lequel comparer.
        :return: True si les deux mots sont dans l'ordre alphabétique False
            sinon.
        """
        # Pour chaque lettre des deux mots
        for c1, c2 in zip(self.word, other.word):
            # Si les lettre sont dans l'ordre alphabétique, les mots sont alors
            # dans l' ordre alphabétique. Si les lettre sont dans l'ordre
            # alphabétique inverse, les mots ne sont pas dans l'ordre
            # alphabétique. Si les lettres sont les mêmes continuer à la
            # prochaine lettre.
            if ord(c1) < ord(c2):
                return True
            elif ord(c1) > ord(c2):
                return False
        
        # Les mots ont la même racine, donc si le mot est plus petit, les mots
        # sont dans l'ordre alphabétique.
        return len(self.word) < len(other.word)
    
    def __le__(self, other: 'Word') -> bool:
        """
        Opérateur plus petit ou égal à du mot.
        :param other: L'autre mot avec lequel comparer.
        :return: True si les deux mots sont dans l'ordre alphabétique ou sont
            les mêmes False sinon.
        """
        # Pour chaque lettre des deux mots
        for c1, c2 in zip(self.word, other.word):
            # Si les lettre sont dans l'ordre alphabétique, les mots sont alors
            # dans l' ordre alphabétique. Si les lettre sont dans l'ordre
            # alphabétique inverse, les mots ne sont pas dans l'ordre
            # alphabétique. Si les lettres sont les mêmes continuer à la
            # prochaine lettre.
            if ord(c1) < ord(c2):
                return True
            elif ord(c1) > ord(c2):
                return False
        
        # Les mots ont la même racine, donc si le mot est plus petit ou égaux,
        # les mots sont dans l'ordre alphabétique ou les mots sont les mêmes.
        return len(self.word) <= len(other.word)
    
    def __gt__(self, other: 'Word') -> bool:
        """
        Opérateur plus grand que du mot.
        :param other: L'autre mot avec lequel comparer.
        :return: True si les deux mots sont dans l'ordre alphabétique inverse
            False sinon.
        """
        # Les mots sont dans l'ordre alphabétique inverse, veut dire que les
        # mots ne sont ni dans l'ordre alphabétique ni les mêmes.
        return not (self <= other)
    
    def __ge__(self, other: 'Word') -> bool:
        """
        Opérateur plus grand ou égal à du mot.
        :param other: L'autre mot avec lequel comparer.
        :return: True si les deux mots sont dans l'ordre alphabétique inverse ou
            sont les mêmes False sinon.
        """
        # Les mots sont dans l'ordre alphabétique inverse ou les mêmes, veut
        # dire que les mots ne sont pas dans l'ordre alphabétique.
        return not (self < other)