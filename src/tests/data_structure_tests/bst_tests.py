import unittest

from data_structures import BinarySearchTree


BST_SAMPLE_1: BinarySearchTree[int] = BinarySearchTree(
    left = BinarySearchTree(
        left = BinarySearchTree(1),
        value = 4,
        right = BinarySearchTree(12)
    ),
    value = 17,
    right = BinarySearchTree(
        left = BinarySearchTree(14),
        value = 25,
        right = BinarySearchTree(29)
    )
)

BST_SAMPLE_2: BinarySearchTree[int] = BinarySearchTree(
    left = BinarySearchTree(-8),
    value = 15,
    right = BinarySearchTree(
        left = BinarySearchTree(
            left = BinarySearchTree(18),
            value = 19,
            right = BinarySearchTree(25)
        ),
        value = 29,
        right = None
    )
)

SAMPLE_NUMBERS: int = [
    10240, -41981, 90117, 4111, -82918, 81948, 20529, -42952, -62919, 82490,
    72250, -53695, -84927, -86967, 30282, 12882, 87635, -76712, 52827, 7776,
    -7066, -34201, -98192, -96134, 34950, 6279, -29543, 31392, -49500, -76630,
    48810, -89927, 99516, 85181, 93375, -85824, -12605, -64827, -816, -48422,
    72412, 17117, 47843, -78619, 38126, -55057, 72944, -90373, -91395, 14591,
    30468, 13577, 42765, -45804, 25878, -88581, -32998, -3293, 807, -2774,
    60203, 77100, -34505, -90824, -63169, -46272, 78663, 86348, 19789, 80220,
    -77476, -17060, -93345, -41112, 91505, 61300, -62087, 50053, -59002, 34694,
    1419, -89714, 74652, -83555, -74847, -29277, -79449, 18354, 90042, -19523,
    -7744, 2499, 43460, -68157, 32206, 6113, 12772, -45580, -30727, 53755, 0
]


class TestBinarySearchTree(unittest.TestCase):
    """
    Test de la structure de données ABR.
    """
    
    def test_is_leaf(self) -> None:
        """Test de l'assesseur is_leaf."""
        self.assertFalse(BST_SAMPLE_1.is_leaf)
        self.assertFalse(BST_SAMPLE_2.is_leaf)
        self.assertTrue(BinarySearchTree(5).is_leaf)
    
    def test_is_node(self) -> None:
        """Test de l'assesseur is_node."""
        self.assertTrue(BST_SAMPLE_1.is_node)
        self.assertTrue(BST_SAMPLE_2.is_node)
        self.assertFalse(BinarySearchTree(5).is_node)

    def test_has_left(self) -> None:
        """Test de l'assesseur has_left."""
        self.assertTrue(BST_SAMPLE_1.has_left)
        self.assertTrue(BST_SAMPLE_2.has_left)
        self.assertFalse(BinarySearchTree(5).has_left)

    def test_has_right(self) -> None:
        """Test de l'assesseur has_right."""
        self.assertTrue(BST_SAMPLE_1.has_right)
        self.assertTrue(BST_SAMPLE_2.has_right)
        self.assertFalse(BinarySearchTree(5).has_right)

    def test_width(self) -> None:
        """Test de la fonction de la taille de l'ABR."""
        self.assertEqual(BST_SAMPLE_1.width(), 7)
        self.assertEqual(BST_SAMPLE_2.width(), 6)
        self.assertEqual(BinarySearchTree(5).width(), 1)
    
    def test_height(self) -> None:
        """Test de la fonction de la hauteur de l'ABR."""
        self.assertEqual(BST_SAMPLE_1.height(), 2)
        self.assertEqual(BST_SAMPLE_2.height(), 3)
        self.assertEqual(BinarySearchTree(5).height(), 0)
    
    def test_contains_value(self) -> None:
        """Test de recherche."""
        self.assertTrue(BST_SAMPLE_1.contains_value(4))
        self.assertTrue(BST_SAMPLE_1.contains_value(17))
        self.assertTrue(BST_SAMPLE_1.contains_value(29))
        self.assertFalse(BST_SAMPLE_1.contains_value(-2))
        self.assertFalse(BST_SAMPLE_1.contains_value(3))
        self.assertFalse(BST_SAMPLE_1.contains_value(100))

        self.assertTrue(BST_SAMPLE_2.contains_value(-8))
        self.assertTrue(BST_SAMPLE_2.contains_value(15))
        self.assertTrue(BST_SAMPLE_2.contains_value(29))
        self.assertFalse(BST_SAMPLE_2.contains_value(-2))
        self.assertFalse(BST_SAMPLE_2.contains_value(3))
        self.assertFalse(BST_SAMPLE_2.contains_value(100))

        self.assertTrue(BinarySearchTree(5).contains_value(5))
        self.assertFalse(BinarySearchTree(5).contains_value(0))
    
    def test_add_value(self) -> None:
        """Test d'ajout."""
        bst = BinarySearchTree(666)
        for sample in SAMPLE_NUMBERS:
            self.assertFalse(bst.contains_value(sample))
            bst.add_value(sample)
            self.assertTrue(bst.contains_value(sample))
        
        for sample in SAMPLE_NUMBERS:
            self.assertTrue(bst.contains_value(sample))
    
    def test_bfs(self) -> None:
        """Test du parcours en largeur."""
        self.assertEqual(BST_SAMPLE_1.bfs(), [17, 4, 25, 1, 12, 14, 29])
        self.assertEqual(BST_SAMPLE_2.bfs(), [15, -8, 29, 19, 18, 25])

    def test_in_order_walk(self) -> None:
        """Test du parcours infixe."""
        self.assertEqual(BST_SAMPLE_1.in_order_walk(),
            [1, 4, 12, 17, 14, 25, 29])
        self.assertEqual(BST_SAMPLE_2.in_order_walk(), [-8, 15, 18, 19, 25, 29])

    def test_pre_order_walk(self) -> None:
        """Test du parcours préfixe."""
        self.assertEqual(BST_SAMPLE_1.pre_order_walk(),
            [17, 4, 1, 12, 25, 14, 29])
        self.assertEqual(BST_SAMPLE_2.pre_order_walk(),
            [15, -8, 29, 19, 18, 25])

    def test_post_order_walk(self) -> None:
        """Test du parcours préfixe."""
        self.assertEqual(BST_SAMPLE_1.post_order_walk(),
            [1, 12, 4, 14, 29, 25, 17])
        self.assertEqual(BST_SAMPLE_2.post_order_walk(),
            [-8, 18, 25, 19, 29, 15])