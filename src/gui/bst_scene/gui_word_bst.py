import pygame

from data_structures import BinarySearchTree
from scrapple import Word


FONT = pygame.font.SysFont(pygame.font.get_default_font(), 24)


def _draw_text(target: pygame.Surface, text: str,
        centered_position: pygame.Vector2) -> None:
    text_surf = FONT.render(text, True, (200, 200, 200))
    highlight_surf = FONT.render(text, True, (0, 0, 0))
    position = centered_position - pygame.Vector2(text_surf.get_size())/2

    target.blit(highlight_surf, position + pygame.Vector2(0, 1))
    target.blit(highlight_surf, position + pygame.Vector2(0, -1))
    target.blit(highlight_surf, position + pygame.Vector2(1, 0))
    target.blit(highlight_surf, position + pygame.Vector2(-1, 0))
    target.blit(text_surf, position)


def _draw_bst(target: pygame.Surface, bst: BinarySearchTree[Word],
    position: pygame.Vector2, x_gap: float, y_gap: float) -> None:

    if bst.has_left:
        left_position = position + pygame.Vector2(-x_gap, y_gap)
        pygame.draw.line(target, (100, 100, 100), left_position, position, 2)
        _draw_bst(target, bst.left, left_position, x_gap/2, y_gap)
    if bst.has_right:
        right_position = position + pygame.Vector2(x_gap, y_gap)
        pygame.draw.line(target, (100, 100, 100), right_position, position, 2)
        _draw_bst(target, bst.right, right_position, x_gap/2, y_gap)

    pygame.draw.circle(target, (200, 100, 100), position, 10, 4)
    _draw_text(target, bst.value.word, position)


class GuiWordBST:
    def __init__(self, bst: BinarySearchTree[Word], position: pygame.Vector2
            ) -> None:
        self.bst: BinarySearchTree[Word] = bst
        self.position: pygame.Vector2 = position
    
    def render_to(self, target: pygame.Surface) -> None:
       _draw_bst(target, self.bst, self.position, 150, 75)