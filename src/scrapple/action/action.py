from ..letter import Letter
from ..player import Player


class Action:
    """Une action que le jouer fait pendant son tour."""

    def __init__(self, player: Player) -> None:
        """
        Constructeur d'une action.
        :param player: Le joueur à l'origine de l'action.
        """
        self.player: Player = player
        self.letters_drawn: list[Letter] = list()