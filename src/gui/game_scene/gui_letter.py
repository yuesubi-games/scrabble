import pygame

from assets import *
from scrapple import *


def render_letter_to(target: pygame.Surface, letter: Letter,
        position: pygame.Vector2, rotation: float = 0.0):
    """
    Dessiner le plateau du scrapple.
    :param target: La surface sur laquelle dessiner.
    :param letter: La lettre a dessiner.
    :param position: La position à laquelle dessiner la lettre.
    """
    is_blank = isinstance(letter, JokerLetter)
    if is_blank:
        is_blank = not letter.has_chose_character

    # Dessiner le fond de la lettre
    surf = pygame.Surface((GUI_UNIT, GUI_UNIT), pygame.SRCALPHA)
    surf.fill(LETTER_COLOR)
    
    # Dessiner le caractère
    if not is_blank:
        letter_surf = LETTER_CHAR_FONT.render(letter.character, True,
            LETTER_FONT_COLOR)
        letter_pos = (pygame.Vector2(surf.get_size())/2 -
            pygame.Vector2(letter_surf.get_size())/2)
        surf.blit(letter_surf, letter_pos)
    
    if letter.points > 0:
        # Dessiner le nombre qui indique les points
        points_surf = LETTER_POINT_FONT.render(str(letter.points), True,
            LETTER_FONT_COLOR)
        points_pos = (pygame.Vector2(surf.get_size()) -
            pygame.Vector2(points_surf.get_size()) -
            pygame.Vector2(LETTER_BORDER_WIDTH, LETTER_BORDER_WIDTH)*2)
        surf.blit(points_surf, points_pos)
    
    rect = pygame.Rect((0, 0), surf.get_size())
    pygame.draw.rect(surf, LETTER_BORDER_COLOR, rect, LETTER_BORDER_WIDTH)

    if rotation != 0.0:
        surf = pygame.transform.rotate(surf, rotation)
    
    target.blit(surf, position - pygame.Vector2(surf.get_size())/2)