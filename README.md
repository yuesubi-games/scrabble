# Scrapple

<img src="https://gitlab.com/yuesubi-games/scrapple/-/raw/main/res/screenshots/1.png" width="640">

## Run
You **must** have `python3` and `pygame2` available on your system to run this
program. (These are **not the exact names** of the packages)

Git clone this repo or download the zip file and extract it.

Go into the cloned or extracted directory and run `src/scrapple-gui.py` with
`python3`

Example :
```sh
git clone https://gitlab.com/yuesubi-games/scrapple.git
cd scrapple
python3 src/scrapple-gui.py
```

## Sources
This project uses a french dictionary/word list of existing french words (
`/res/dictionaries/fr.txt`) from http://www.3zsoftware.com/fr/listes.php.


## Requis
- [x] Contenir au moins 1 arbre binaire de recherche 
- [x] Permettre une visualisation de sa structure plus avancée que celle déjà
    vue.
- [x] Créer une ou plusieurs fonctions de parcours (infixe,etc..) et vérifier
    que le fonctionnement est correct.
- [x] permettre de rechercher si une étiquette existe.
- [x] pourvoir ajouter des éléments à l’arbre binaire de recherche
- [x] afficher en permanence la hauteur et la taille de l’arbre.
- [ ] une preuve de correction, terminaison et complexité est demandée pour l’
    ajout ou la recherche dans l’arbre.
- [x] Ne pas oublier les spécifications de vos algorithmes.
- [x] Une fois que le reste est fonctionnel : Utilisation de tkinter ou pygame
    pour visualiser l’arbre et/ou le reste du projet.


## Cahier des charges
- Pouvoir jouer au scrapple
- Utilisation d'un ABR pour vérifier si le mot existe 
- Pouvoir voir le dictionnaire du scrapple, c-a-d l'ABR, avec taille, hauteur,
    ajout et parcours.
- Spécification des algorithmes 
- Une preuve de correction, terminaison et complexité pour l’ajout et la
    recherche dans l’arbre


## Progression
* 2023/01/04 1h30 Ajout des structures de données
* 2023/01/06 1h30 Ajout de classes pour les lettres du Scrapple
* 2023/01/07 2h00 Ajout de classes pour le dictionnaire et les mots.
* 2023/01/07 1h00 Ajout des ressources, de tests et du dictionnaire français.
* 2023/01/08 1h10 Ajout du sac de pioche
* 2023/01/08 0h25 Ajout d'actions pour le Scrapple
* 2023/01/08 0h35 Ajout de classe pour les joueurs
* 2023/01/08 0h35 Ajout du plateau de jeu
* 2023/01/15 3h00 Création de l'algorithme de détection des mots formés par les
    lettres.
* 2023/01/15 0h30 Ajout de la classe Scrapple pour interagir avec le jeu
* 2023/01/18 1h00 Corrections de bug dans la partie Scrapple et ajout de
    widgets, scenes et gestionnaire d'évènement pour l'interface graphique.
* 2023/01/20 1h00 Ajout d'une classe pour la structure de données Pile et de
    scenes pour l'interface graphique.
* 2023/01/21 0h30 Changement de la classe Scrapple. Les actions contiennent
    maintenant les joueurs qui les ont faits.
* 2023/01/22 3h00 Prototype simple de la partie graphique. Découverte de
    beaucoup de bugs du jeu et résolution de certains d'entre eux.
* 2023/01/23 1h00 Modification de comment les lettres sont dessinées et ajout d'
    animations pour les lettres déplacées. Beaucoup de tests.
* 2023/01/29 0h15 Résolution du bug avec un seule lettre
* 2023/01/30 0h30 Création de la scène pour visualiser l'ABR
* 2023/02/01 1h30 Re-factorisation du code pour la partie graphique du scrapple
    pour avoir les différents modes du jeu regroupés ensembles.
* 2023/02/09 0h30 Ajout de tests pour la pile et correction d'une erreur dans le
    nom des fichiers
* 2023/02/12 1h00 Ajout d'un menu pour pouvoir choisir la lettre à utiliser pour
    le joker
* 2023/02/12 1h00 Création d'un champ de texte pour l'IHM.
* 2023/02/18 0h30 Ajout des boutons pour ajouter et chercher des éléments dans
    l'ABR
* 2023/02/22 0h45 Création de notifications au lieu d'afficher en console les
    information, dans la scène de l'ABR
* 2023/02/22 0h45 Création d'un menu d'aide pour le scrapple
* 2023/02/24 0h20 Ajout de la vérification de si les mot sont connectés aux
    autres ou au centre pour le premier tour
* 2023/02/26 3h00 Ajout d'un menu de fin de partie et quelques finitions

prevision: ~30h
véritable: 28h50


## Architecture
```txt
+--------+--------+
|  gui   |  cli   |
+--------+--------+
|      game       |
+-----------------+
|    scrapple     |
+-----------------+
| data structures |
+-----------------+
```