import pygame


class Polygon:
    def __init__(self) -> None:
        """Constructeur, initialise les attributs pour qu'ils soient vides."""
        
        # Attributs contenant la localisation du polygone
        self.position: pygame.Vector2 = pygame.Vector2()
        self.rotation: float = 0.0
        self.size: float = 1.0
                
        # Tout les points pouvant êtres utiliser pour faire des triangles ou la
        # bordure du polygone
        self.vertices: list[pygame.Vector2] = list()
        
        # Les indices des point utilisés pour le contour du polygone
        self.indices: list[int] = list()
        # Les indices des coins des triangles qui composent le polygone, is
        # sont nécessaires pour la detection de collision
        self.triangles: list[tuple[int, int, int]] = list()

    def collides_with_point(self, point: pygame.Vector2) -> bool:
        """
        Regarde si le point est dans le polygone.
        :param point: Le point à vérifier.
        :return: Vrai si le point est dans le polygone.
        """
        
        # Transformation de tout les points de cordonnées locales au cordonnées
        # globales
        polygon_vertices = self.get_world_position_vertices()

        # Enregistre si le point est dans le polygone
        is_point_in_polygon = False

        for triangle in self.triangles:
            is_point_in_triangle = True
            vertex_index = 0

            # Ici la boucle while est utilisée car dans la plus part des cas on
            # découvre que le point n'est pas dans le triangle avant d'avoir
            # fait les trois boucle. Et il y a beaucoup plus de chances que le
            # point ne soit pas dans le triangle.
            while vertex_index < 3 and is_point_in_triangle:
                # Trouver le point, le suivant et celui encore après
                vertex_0 = polygon_vertices[triangle[vertex_index]]
                vertex_1 = polygon_vertices[triangle[(vertex_index + 1) % 3]]
                vertex_2 = polygon_vertices[triangle[(vertex_index + 2) % 3]]

                # Trouver un vecteur normal au coté du triangle
                side_vector = vertex_1 - vertex_0
                side_normal = pygame.Vector2(-side_vector.y, side_vector.x)

                # Pour savoir si le point est du bon coté du coté du triangle
                # les produits scalaires suivants sont nécessaires:
                # - side_normal avec (point - vertex_0)
                # - side_normal avec (vertex_2 - vertex_0)
                #
                #          vertex_2
                #             x
                #            / \
                #           /   \        x point
                #          /     \
                #         /       \
                #        /         \
                #       /           \
                #      / side_vector \
                #     x<--------------x
                #  vertex_1   |    vertex_0
                #             |
                #             | side_normal
                #             |
                #             V
                #
                # Dire que le point est dans le triangle peut être formulé
                # comme: le point doit être du bon coté de chaque coté du
                # triangle.
                #
                # Pour que le point soit bon coté du coté du triangle il
                # faut que l'un des deux suivants soit vrais:
                # - Le point est sur la ligne du triangle (le produit scalaire
                #   est zéro)
                # - Le point et le point du triangle sont du même coté du
                #   coté du triangle (les deux produits scalaires ont le même
                #   signe)

                dot_with_point = side_normal.dot(point - vertex_0)
                dot_with_vertex = side_normal.dot(vertex_2 - vertex_0)

                is_point_in_triangle = is_point_in_triangle and (
                    (dot_with_point == 0.0) or
                    (dot_with_point > 0.0 and dot_with_vertex > 0.0) or
                    (dot_with_point < 0.0 and dot_with_vertex < 0.0)
                )
                
                # Incrémentation du compteur de la boucle
                vertex_index += 1
            
            is_point_in_polygon = is_point_in_polygon or is_point_in_triangle
        
        return is_point_in_polygon
    
    def collide_with_polygon(self, other_polygon) -> bool:
        """
        Regarde si les deux polygones se touchent.
        :param other_polygon: L'autre polygone.
        :return: Vrai si le point est dans le polygone.
        """

        is_colliding = False

        # Vérifie si un des coins du polygone est dans l'autre polygone
        polygone_vertices = self.get_world_position_vertices()
        for vertex_index in self.indices:
            vertex = polygone_vertices[vertex_index]
            is_colliding = (
                is_colliding or
                other_polygon.collides_with_point(vertex)
            )

        # Vérifie si un des coins de l'autre polygone est dans le polygone
        polygone_vertices = other_polygon.get_world_position_vertices()
        for vertex_index in other_polygon.indices:
            vertex = polygone_vertices[vertex_index]
            is_colliding = (
                is_colliding or
                self.collides_with_point(vertex)
            )

        return is_colliding
    
    def render_to(self, target_surface: pygame.Surface, color: pygame.Color,
                  line_width: int = 0) -> None:
        """
        Dessiner le polygone.
        :param target_surface: La surface sur laquelle dessiner le polygone.
        :param color: La couleur du polygone.
        :param line_width: La taille de la ligne de contour du polygone. Si
            elle est de 0, le polygone est rempli.
        """
        polygon_vertices = self.get_world_position_vertices()
        pygame.draw.polygon(target_surface, color, polygon_vertices,
                            line_width)
    
    def get_world_position_vertices(self) -> list[pygame.Vector2]:
        """
        Récupérer tout les points transformés en cordonnées globales.
        :return: Le nouveaux points.
        """
        new_polygon_vertices = list()

        for original_vertex in self.vertices:
            rotated_vertex = original_vertex.rotate(self.rotation)
            scaled_vertex = rotated_vertex * self.size
            new_polygon_vertices.append(self.position + scaled_vertex)

        return new_polygon_vertices
