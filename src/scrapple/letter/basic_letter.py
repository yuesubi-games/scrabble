from assets import POINTS_OF_LETTERS
from .letter import Letter


class BasicLetter(Letter):
    """Une lettre basique du scrapple."""
    
    def __init__(self, character: str) -> None:
        """Constructeur d'une lettre basique."""
        super().__init__()
        self._character: str = character
        self._points: int = POINTS_OF_LETTERS[character]
    
    def __repr__(self) -> str:
        return f"BasicLetter <'{self._character}'>"

    @property
    def character(self) -> str:
        return self._character
    
    @property
    def points(self) -> int:
        return self._points
    
    def __repr__(self) -> str:
        return f"BasicLetter <'{self._character}'>"