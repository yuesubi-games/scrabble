import enum
import math
import pygame
from functools import partial

from assets import *
from scrapple import *

from .. import cache
from ..event_manager import EventManager
from ..scene_manager import Scene, SceneId, SceneManager
from ..widgets import Anchor, Text, TextButton, Fit, Frame

from .gui_board import GuiBoard
from .gui_deck import GuiDeck
from .gui_letter import render_letter_to


class State(enum.Enum):
    NORMAL: int = 0
    PLACING_LETTERS: int = 1
    TRASHING_LETTERS: int = 2
    CONFIRMING_EXIT: int = 3
    CONFIRMING_GO_BACK: int = 4
    GETTING_HELP: int = 5
    CHOOSING_JOKER_LETTER: int = 6


class GameScene(Scene):
    """Le menu principal de l'application."""

    def __init__(self) -> None:
        """Constructeur."""
        super().__init__()

        self.dictionary: Dictionary = cache.FrenchDictionary.acquire()
        self.scrapple: Scrapple = Scrapple(
            [f"Player {l}" for l in range(1, 3)],
            self.dictionary
        )
        
        # Plateau et main du joueur
        self.gui_board: GuiBoard = GuiBoard(self.scrapple.board,
            pygame.Vector2(5, 5), GUI_UNIT
        )
        self.gui_deck: GuiDeck = GuiDeck(
            self.scrapple.player_who_plays_next.deck,
            pygame.Vector2(5, 10 + GUI_UNIT*15)
        )
        
        # Variables de contrôle de l'état du jeu
        self.state: State = State.NORMAL

        # Variables pour le déplacement de lettres
        self.moving_letter: Letter | None = None
        self.moving_letter_pos: pygame.Vector2 = pygame.Vector2(0, 0)
        self.moving_letter_time: float = 0.0

        # Variable pour enregistrer les lettres à défausser
        self.temporary_trash: list[Letter] = list()

        ###############################################
        # Variables pour le mode CHOOSING_JOKER_LETTER

        # Le mode depuis lequel la selection de la lettre à commencée
        self.choosing_joker_letter_prev_state: State = State.NORMAL
        self.choosing_joker_letter_modified_letter: JokerLetter | None = None

        # Le menu pour choisir la lettre à utiliser
        self.choosing_joker_letter_menu: Frame = Frame(
            pygame.Vector2(0, 0), Anchor.C,
            pygame.Vector2(36*5, 36*6), Fit.NONE,
            background_color=pygame.Color(50, 50, 50),
            border_color=pygame.Color(75, 75, 75),
            border_width=2,
            children=[
                TextButton(
                    pygame.Vector2(5 + 35*(l % 5), 5 + 35*(l // 5)), Anchor.NW,
                    pygame.Vector2(30, 30), Fit.NONE,
                    f"[{chr(ord('A') + l)}]",
                    font_color=pygame.Color(200, 200, 200),
                    font_size=22,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=partial(
                        self.choosing_joker_letter_letter_button_cmd,
                        chr(ord('A') + l)
                    )
                )
                for l in range(1 + ord('Z') - ord('A'))
            ]
        )
        
        # Menu d'aide
        self.help_menu: Frame = Frame(
            pygame.Vector2(0, 0), Anchor.C,
            pygame.Vector2(440, 430), Fit.NONE,
            background_color=pygame.Color(50, 50, 50),
            border_color=pygame.Color(75, 75, 75),
            border_width=2,
            children=[
                # Message d'aide
                Text(
                    pygame.Vector2(0, 28*(l - 7)), Anchor.C,
                    text=line,
                    font_size=24,
                    font_color=pygame.Color(200, 200, 200)
                )
                for l, line in enumerate([
                    "Pour placer des lettres, glissez puis déposez les lettres",
                    "sur le plateau. Le bouton retirer permet de remettre les",
                    "lettres dans sa main et le bouton placer permet de",
                    "placer les lettres définitivement. Les lettres vides sont",
                    "des joker, faites un clique droit dessus pour modifier la",
                    "letter.",
                    "Pour défausser des letters (ou passer son tour), il faut",
                    "appuyer sur le bouton défausser. Vous pouvez alors",
                    "cliquer sur les lettres à défausser. Vous pouvez annuler",
                    "ou confirmer avec les boutons du même nom.",
                    "Le bouton quitter permet de revenir au menu principal.",
                    "Pour les règles du jeu consultez :",
                    "https://regledujeu.fr/scrabble/"
                ])
            ] + [
                # Bouton ok
                TextButton(
                    pygame.Vector2(0, 180), Anchor.C,
                    pygame.Vector2(50, 30), Fit.NONE,
                    "Ok",
                    font_color=pygame.Color(200, 200, 200),
                    font_size=24,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.getting_help_state_end
                )
            ]
        )
         
        # Menu pour quitter la partie
        self.quit_menu: Frame = Frame(
            pygame.Vector2(0, 0), Anchor.C,
            pygame.Vector2(275, 100), Fit.NONE,
            background_color=pygame.Color(50, 50, 50),
            border_color=pygame.Color(75, 75, 75),
            border_width=2,
            children=[
                # Message de confirmation
                Text(
                    pygame.Vector2(0, -15), Anchor.C,
                    text="Voulez-vous vraiment quitter ?",
                    font_size=24,
                    font_color=pygame.Color(200, 200, 200)
                ),
                # Bouton OUI
                TextButton(
                    pygame.Vector2(40, 15), Anchor.C,
                    pygame.Vector2(50, 30), Fit.NONE,
                    "Oui",
                    font_color=pygame.Color(200, 200, 200),
                    font_size=24,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.quit_menu_yes_button_cmd
                ),
                # Bouton NON
                TextButton(
                    pygame.Vector2(-40, 15), Anchor.C,
                    pygame.Vector2(50, 30), Fit.NONE,
                    "Non",
                    font_color=pygame.Color(200, 200, 200),
                    font_size=24,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.quit_menu_no_button_cmd
                )
            ]
        )

        # Boutons du mode normal
        self.normal_button_frame: Frame = Frame(
            pygame.Vector2(0, 0), Anchor.SE,
            pygame.Vector2(0, 0), Fit.NONE,
            children=[
                # Bouton pour retirer les lettres placées
                #TextButton(
                #    pygame.Vector2(-110, -5), Anchor.SE,
                #    pygame.Vector2(100, 30), Fit.NONE,
                #    "Annuler",
                #    font_color=pygame.Color(0, 0, 0),
                #    font_size=26,
                #    background_color=pygame.Color(100, 100, 100),
                #    border_color=pygame.Color(100, 100, 100),
                #    border_width=2,
                #    command=self.frame_cancel_button_cmd
                #),
                # Bouton d'aide
                TextButton(
                    pygame.Vector2(-110, -40), Anchor.SE,
                    pygame.Vector2(100, 30), Fit.NONE,
                    "Aide",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.frame_help_button_cmd
                ),
                # Bouton pour défausser des lettres
                TextButton(
                    pygame.Vector2(-5, -40), Anchor.SE,
                    pygame.Vector2(100, 30), Fit.NONE,
                    "Défausser",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.normal_frame_trash_button_cmd
                ),
                # Bouton pour quitter la partie
                TextButton(
                    pygame.Vector2(-5, -5), Anchor.SE,
                    pygame.Vector2(100, 30), Fit.NONE,
                    "Quitter",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.frame_quit_button_cmd
                )
            ]
        )

        # Boutons du mode pour poser des lettres
        self.placing_letters_button_frame: Frame = Frame(
            pygame.Vector2(0, 0), Anchor.SE,
            pygame.Vector2(0, 0), Fit.NONE,
            children=[
                # Bouton pour retirer les lettres placées
                TextButton(
                    pygame.Vector2(-110, -5), Anchor.SE,
                    pygame.Vector2(100, 30), Fit.NONE,
                    "Retirer",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.frame_remove_button_cmd
                ),
                # Bouton pour jouer les lettres placées
                TextButton(
                    pygame.Vector2(-5, -5), Anchor.SE,
                    pygame.Vector2(100, 30), Fit.NONE,
                    "Placer",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.frame_place_button_cmd
                )
            ]
        )

        # Boutons du mode pour défausser
        self.trashing_letter_button_frame: Frame = Frame(
            pygame.Vector2(0, 0), Anchor.SE,
            pygame.Vector2(0, 0), Fit.NONE,
            children=[
                # Bouton pour récupérer les lettres de la poubelle temporaire
                TextButton(
                    pygame.Vector2(-110, -5), Anchor.SE,
                    pygame.Vector2(100, 30), Fit.NONE,
                    "Annuler",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.trashing_letter_frame_cancel_button_cmd
                ),
                # Bouton pour défausser les lettres
                TextButton(
                    pygame.Vector2(-5, -5), Anchor.SE,
                    pygame.Vector2(100, 30), Fit.NONE,
                    "Confirmer",
                    font_color=pygame.Color(0, 0, 0),
                    font_size=26,
                    background_color=pygame.Color(100, 100, 100),
                    border_color=pygame.Color(100, 100, 100),
                    border_width=2,
                    command=self.trashing_letter_frame_trash_button_cmd
                )
            ]
        )

        # Dictionnaire qui associe un joueur à un texte de score
        self.score_texts: dict[Player, Text] = {
            player: Text(
                pygame.Vector2(100, 25*p), Anchor.NW,
                text=("* " if p == 0 else "") + "score : 0",
                font_size=24,
                font_color=pygame.Color(200, 200, 200)
            )
            for p, player in enumerate(self.scrapple._players)
        }

        # Texte avec le nom du joueur
        self.player_name_frame: Frame = Frame(
            pygame.Vector2(-5, 5), Anchor.NE,
            pygame.Vector2(205, 300), Fit.NONE,
            background_color=pygame.Color(100, 100, 100),
            children=list(self.score_texts.values()) + [
                Text(
                    pygame.Vector2(0, 25*p), Anchor.NW,
                    text=player.name,
                    font_size=24,
                    font_color=pygame.Color(200, 200, 200)
                )
                for p, player in enumerate(self.scrapple._players)
            ]
        )

        # Cadre principal
        self.frame: Frame = Frame(
            pygame.Vector2(0, 0), Anchor.NW,
            pygame.Vector2(1, 1), Fit.NONE,
            children=[
                self.normal_button_frame,
                self.player_name_frame
            ]
        )
    
    ############################################################################
    # Méthodes de la scène
    
    def register_events(self, event_manager: EventManager) -> None:
        # Actualiser les widgets
        self.frame.process_events(event_manager)

        # Lancer la fonction qui correspond à l'état de la scene
        if self.state == State.NORMAL:
            self.normal_state_handle_events(event_manager)
        elif self.state == State.PLACING_LETTERS:
            self.placing_letters_state_handle_events(event_manager)
        elif self.state == State.TRASHING_LETTERS:
            self.is_trashing_letters_handle_events(event_manager)
 
    def update_by(self, delta_time: float) -> None:
        self.moving_letter_time += delta_time

        self.frame.size = pygame.Vector2(pygame.display.get_window_size())
        self.frame.update(delta_time)

    def render_to(self, target_surface: pygame.Surface) -> None:
        self.gui_board.render_to(target_surface)
        self.gui_deck.render_to(target_surface)

        if self.moving_letter is not None:
            tilt = math.sin(math.pi * self.moving_letter_time)
            tilt *= LETTER_MOVING_TILT

            render_letter_to(target_surface, self.moving_letter,
                self.moving_letter_pos, tilt)

        self.frame.render(target_surface)
    
    ############################################################################
    # Méthodes générales
    
    def goto_next_turn(self) -> None:
        """Passer au prochain tour."""

        # Actualiser les lettres
        self.gui_board.clear_temporary_letters()
        self.moving_letter = None
        self.gui_deck = GuiDeck(self.scrapple.player_who_plays_next.deck,
            pygame.Vector2(5, 10 + GUI_UNIT*15))

        self.normal_state_start()

        # Mettre a jour les scores
        for player, score in self.scrapple.scores.items():
            score_str = str()
            if player == self.scrapple.player_who_plays_next:
                score_str += "* "

            score_str += f"score : {score}"
            self.score_texts[player].change_text(score_str)
        
        # Vérification du vainqueur
        if self.scrapple.is_game_finished:
            SceneManager.switch_scene(SceneId.SCORE, self.scrapple.scores)
            cache.FrenchDictionary.release()

    def frame_help_button_cmd(self) -> None:
        self.normal_state_end()
        self.getting_help_state_start()
    
    def frame_cancel_button_cmd(self) -> None:
        self.normal_state_end()
        self.confirming_go_back_state_start()
    
    def normal_frame_trash_button_cmd(self) -> None:
        self.normal_state_end()
        self.trashing_letters_state_start()
    
    def trashing_letter_frame_cancel_button_cmd(self) -> None:
        for trashed_letter in self.temporary_trash:
            self.gui_deck.push_letter(trashed_letter)
        self.temporary_trash.clear()
        
        self.trashing_letters_state_end()
        self.normal_state_start()
    
    def frame_place_button_cmd(self) -> None:
        try:
            self.scrapple.player_place_letters(self.gui_board.temporary_letters)
            self.placing_letters_state_end()
            self.goto_next_turn()
        except ScrappleException as _:
            pass
    
    def trashing_letter_frame_trash_button_cmd(self) -> None:
        self.scrapple.player_trash_letters(self.temporary_trash)
        self.temporary_trash.clear()

        self.trashing_letters_state_end()
        self.goto_next_turn()
    
    def frame_remove_button_cmd(self) -> None:
        for temp_letter in self.gui_board.temporary_letters:
            self.gui_deck.push_letter(temp_letter)
        self.gui_board.clear_temporary_letters()

        if self.moving_letter is not None:
            self.gui_deck.push_letter(self.moving_letter)
    
    def frame_quit_button_cmd(self) -> None:
        """Commande du bouton pour quitter la partie."""
        self.normal_state_end()
        self.confirming_exit_state_start()
    
    def quit_menu_yes_button_cmd(self) -> None:
        """
        Commande du bouton "Oui" pour la confirmation de quitter la partie.
        """
        SceneManager.switch_scene(SceneId.MENU)
        cache.FrenchDictionary.release()
    
    def quit_menu_no_button_cmd(self) -> None:
        """
        Commande du bouton "Non" pour la confirmation de quitter la partie.
        """
        self.confirming_exit_state_end()
        self.normal_state_start()
    
    ############################################################################
    # Mode NORMAL

    def normal_state_start(self) -> None:
        """Passer en mode normal."""
        self.state = State.NORMAL
        self.frame.add_child(self.normal_button_frame)
    
    def normal_state_end(self) -> None:
        """Arrêter le mode normal."""
        self.frame.remove_child(self.normal_button_frame)
     
    def normal_state_handle_events(self, event_manager: EventManager) -> None:
        """
        Gérer les évènements quand l'utilisateur n'est pas en train de faire
        quelque chose.
        :param event_manager: Le gestionnaire d'événements qui contient les
            entrées actuelles.
        """

        # Si la sourie est au dessus de la main du joueur
        if self.gui_deck.is_position_above(event_manager.mouse_pos):

            # Si il y a un click gauche, essayer de passer en mode placement de
            # lettres
            if event_manager.is_button_pressed(pygame.BUTTON_LEFT):
                self.state = State.PLACING_LETTERS
                self.placing_letters_state_handle_events(event_manager)

                self.frame.remove_child(self.normal_button_frame)
                self.frame.add_child(self.placing_letters_button_frame)
        
            # Si il y a un click droit, essayer de passer en mode configuration
            # de la lettre joker
            if event_manager.is_button_pressed(pygame.BUTTON_RIGHT):
                letter = self.gui_deck.peak_letter_at(event_manager.mouse_pos)
                if isinstance(letter, JokerLetter):
                    self.normal_state_end()
                    self.choosing_joker_letter_state_start(letter)
    
    ############################################################################
    # Mode PLACING_LETTERS

    def placing_letters_state_start(self) -> None:
        """Passer en mode de placement de lettres."""
        self.state = State.PLACING_LETTERS
        self.frame.add_child(self.placing_letters_button_frame)
    
    def placing_letters_state_end(self) -> None:
        """Arrêter le mode de placement de lettres."""
        self.frame.remove_child(self.placing_letters_button_frame)
     
    def placing_letters_state_handle_events(self, event_manager: EventManager
            ) -> None:
        """
        Gérer les évènements quand l'utilisateur est en train de placer des
        lettres.
        :param event_manager: Le gestionnaire d'événements qui contient les
            entrées actuelles.
        """
        # Actualiser la position de la lettre en mouvement pour quelle suive la
        # souris.
        self.moving_letter_pos = event_manager.mouse_pos

        # Si il y a un click gauche
        if event_manager.is_button_pressed(pygame.BUTTON_LEFT):
            # Si il n'y a pas de lettres en mouvement
            if self.moving_letter is None:
                # Essayer de récupérer la lettre de la main du joueur
                self.moving_letter = self.gui_deck.pop_letter_at(
                    event_manager.mouse_pos)
                
                # Si aucune lettre n'a été récupérée, essayer d'en récupérer une
                # du plateau
                if self.moving_letter is None:
                    self.moving_letter = self.gui_board.pop_letter_at(
                        event_manager.mouse_pos)
                
                # Remettre le temps d'animation de la lettre à zéro au cas où
                # une lettre à été sélectionnée
                self.moving_letter_time = 0.0
            
            # Si une lettre est en mouvement
            else:
                # Essayer de poser la lettre sur le plateau
                was_pushed = self.gui_board.try_push_letter(
                    event_manager.mouse_pos, self.moving_letter)
                
                # Si la lettre n'a pas pu être posée, la remettre dans la main
                # du joueur.
                if not was_pushed:
                    self.gui_deck.push_letter(self.moving_letter)
                self.moving_letter = None
        
        # Si il y a un clic droit et qu'aucune lettre n'est en mouvement
        if (event_manager.is_button_pressed(pygame.BUTTON_RIGHT) and
            self.moving_letter is None):

            # Récupérer la lettre sous la sourie
            letter = self.gui_deck.peak_letter_at(event_manager.mouse_pos)
            if letter is None:
                letter = self.gui_board.peak_letter_at(event_manager.mouse_pos)
            
            if isinstance(letter, JokerLetter):
                self.placing_letters_state_end()
                self.choosing_joker_letter_state_start(letter)
        
        # Si aucune lettre n'est déplacée ni placée sur le plateau retourner en
        # mode normal.
        if (len(self.gui_board.temporary_letters) == 0 and
            self.moving_letter is None):
            
            self.state = State.NORMAL
            self.frame.add_child(self.normal_button_frame)
            self.frame.remove_child(self.placing_letters_button_frame)

    ############################################################################
    # Mode TRASHING_LETTERS

    def trashing_letters_state_start(self) -> None:
        """Passer en mode défausser des lettres."""
        self.state = State.TRASHING_LETTERS
        self.frame.add_child(self.trashing_letter_button_frame)
    
    def trashing_letters_state_end(self) -> None:
        """Arrêter le mode défausser des lettres."""
        self.frame.remove_child(self.trashing_letter_button_frame)
     
    def is_trashing_letters_handle_events(self, event_manager: EventManager
            ) -> None:
        """
        Gérer les évènements quand l'utilisateur est en train de défausser des
        lettres.
        :param event_manager: Le gestionnaire d'événements qui contient les
            entrées actuelles.
        """
        if event_manager.is_button_pressed(pygame.BUTTON_LEFT):
            letter = self.gui_deck.pop_letter_at(event_manager.mouse_pos)

            if letter is not None:
                self.temporary_trash.append(letter)
    
    ############################################################################
    # Mode CONFIRMING_EXIT

    def confirming_exit_state_start(self) -> None:
        """Passer en mode confirmation de sortie."""
        self.state = State.CONFIRMING_EXIT
        self.frame.add_child(self.quit_menu)
    
    def confirming_exit_state_end(self) -> None:
        """Arrêter le mode confirmation de sortie."""
        self.frame.remove_child(self.quit_menu)
    
    ############################################################################
    # Mode CONFIRMING_GO_BACK

    def confirming_go_back_state_start(self) -> None:
        """Passer en mode confirmation de retour en arrière."""
        # self.state = State.CONFIRMING_GO_BACK
        # self.frame.add_child(...)
    
    def confirming_go_back_state_end(self) -> None:
        """Arrêter le mode confirmation de retour en arrière."""
        # self.frame.remove_child(...)
    
    ############################################################################
    # Mode GETTING_HELP

    def getting_help_state_start(self) -> None:
        """Passer en mode d'aide."""
        self.state = State.GETTING_HELP
        self.frame.add_child(self.help_menu)
    
    def getting_help_state_end(self) -> None:
        """Arrêter le mode d'aide."""
        self.frame.remove_child(self.help_menu)
        self.normal_state_start()
    
    ############################################################################
    # Mode CHOOSING_JOKER_LETTER

    def choosing_joker_letter_state_start(self, joker_letter: JokerLetter
            ) -> None:
        """Passer en mode selection de lettre pour le joker."""
        self.choosing_joker_letter_prev_state = self.state
        self.state = State.CHOOSING_JOKER_LETTER

        self.choosing_joker_letter_modified_letter = joker_letter
        self.frame.add_child(self.choosing_joker_letter_menu)
    
    def choosing_joker_letter_state_end(self) -> None:
        """Arrêter le mode selection de lettre pour le joker."""
        self.frame.remove_child(self.choosing_joker_letter_menu)
        self.state = self.choosing_joker_letter_prev_state
    
    def choosing_joker_letter_letter_button_cmd(self, letter: str) -> None:
        """
        Commande du bouton pour sélectionner une lettre pour le joker.
        :param letter: La lettre sélectionnée.
        """
        self.choosing_joker_letter_modified_letter.turn_in_character(letter)
        self.choosing_joker_letter_state_end()
        
        if self.state == State.NORMAL:
            self.normal_state_start()
        elif self.state == State.PLACING_LETTERS:
            self.placing_letters_state_start()
        elif self.state == State.TRASHING_LETTERS:
            self.trashing_letters_state_start()