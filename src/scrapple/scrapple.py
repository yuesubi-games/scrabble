from assets import DECK_LETTER_AMOUNT
from data_structures import Stack

from .action import Action, PlaceWordAction, TrashLettersAction
from .bag import Bag
from .board import Board
from .dictionary import Dictionary
from .exceptions import *
from .letter import Letter, JokerLetter
from .player import Player


class Scrapple:
    """Le jeu du Scrapple"""
    
    def __init__(self, player_names: list[str], dictionary: Dictionary) -> None:
        """
        Constructeur du jeu du scrapple.
        :param player_names: Les noms des joueurs de la partie.
        :param dictionary: Le dictionnaire à utiliser pour vérifier les mots.
        """
        
        self.board: Board = Board()
        self._dictionary: Dictionary = dictionary
        self._bag: Bag = Bag()

        # Les joueurs
        self._players: list[Player] = [Player(name) for name in player_names]
        self._player_turn: int = 0

        # Les scores et l'historique des actions
        self._action_history: Stack[Action] = Stack()
        self._scores: dict[Player, int] = {player: 0
            for player in self._players}
        
        # Piocher des lettres du sac et les ajouter à la main de chaque joueur
        for player in self._players:
            picked_letters = self._bag.pick_n_letter(DECK_LETTER_AMOUNT)
            player.deck.extend(picked_letters)
    
    @property
    def amount_of_players(self) -> int:
        """Accesseur du nombre de joueurs qui jouent."""
        return len(self._players)
    
    @property
    def player_who_plays_next(self) -> Player:
        """Récupérer le joueur qui doit jouer ce tour ci."""
        return self._players[self._player_turn] 

    @property
    def is_game_finished(self) -> bool:
        """Si le jeu est fini donne True, False sinon."""
        # Si au moins un des joueurs à toujours des lettres, le jeu n'est pas
        # fini.
        finished = True
        for player in self._players:
            finished = finished and len(player.deck) == 0
        return finished

    @property
    def scores(self) -> dict[Player, int]:
        """Donne un dictionnaire où chaque joueur est associé à son score."""
        return self._scores.copy()
    
    def player_place_letters(self, letters: list[Letter]) -> None:
        """
        Placer les lettres sur le plateau. Peut lancer l'exceptions
        ScrappleInvalidActionException.
        :param letters: Les lettres que le joueur veut placer.
        """
        player = self.player_who_plays_next
        
        # Vérifier que les lettres appartiennent au joueur
        for letter in letters:
            if letter not in player.deck:
                # Le joueur ne possède pas la lettre, une exception est lancée
                raise ScrappleLetterNotOwned(
                    f"Letter {letter} is not owned by {player} !")
                
            if isinstance(letter, JokerLetter):
                # Si une lettre joker n'est pas assignée à un lettre, lacer une
                # exception
                if not letter.has_chose_character:
                    raise ScrappleUsingUnassignedJokerLetter()

        # Trouver les mots formés par les lettres
        formed_words = self.board.detect_formed_words_by_letters(letters)
        words = list()

        # Lancer une exception si aucun mot n'est formé
        if len(formed_words) < 1:
            raise ScrappleNoWordFormed()

        # Pour chaque mot formé vérifier si il est dans le dictionnaire
        for word_formed in formed_words:
            word = word_formed.compute_word()

            # Si le mot n'existe par lancer une exception
            if not self._dictionary.is_word_valid(word):
                raise ScrappleInvalidWordException()
            
            words.append(word)
        
        # Retirer les lettres de la main du joueur
        for letter in letters:
            player.deck.remove(letter)

        # Créer un action et remplir les informations
        action = PlaceWordAction(player)
        action.words_placed = words
        action.letters_used = letters
        action.points_won = self.board.compute_score(formed_words)

        # Piocher des nouvelles lettres
        action.letters_drawn = self._bag.pick_n_letter(len(letters))

        # Ajouter l'action à l'historique
        self._action_history.push(action)

        # Ajouter le score et les nouvelles lettres piochées au joueur
        self._scores[player] += action.points_won
        player.deck.extend(action.letters_drawn)

        self.board._letters.extend(action.letters_used)

        self._goto_next_turn()
    
    def player_trash_letters(self, letters: list[Letter]) -> None:
        """
        Défausser des lettres.
        :param letters: Les lettres à défausser.
        """
        player = self.player_who_plays_next

        # Retirer les lettres de la main du joueur
        for letter in letters:
            player.deck.remove(letter)
        
        # Créer un action et remplir les informations
        action = TrashLettersAction(player)
        action.letters_trashed = letters
        action.player = player

        # Piocher des nouvelles lettres
        action.letters_drawn = self._bag.pick_n_letter(len(letters))

        # Ajouter l'action à l'historique
        self._action_history.push(action)

        # Ajouter le score et les nouvelles lettres piochées au joueur
        player.deck.extend(action.letters_drawn)

        self._goto_next_turn()
    
    def _goto_next_turn(self) -> None:
        # Enregistrer le joueur qui vient de jouer et passer au prochain
        self.previous_player = self.player_who_plays_next
        self._player_turn = (self._player_turn + 1) % self.amount_of_players

        # Continuer de passer au prochain joueur jusqu'a trouver un joueur qui a
        # toujours des lettres. Et s'arrêter si plus personne n'a de lettres.
        while (len(self.player_who_plays_next.deck) == 0 and
            self.player_who_plays_next != self.previous_player):

            self._player_turn = (self._player_turn + 1) % self.amount_of_players