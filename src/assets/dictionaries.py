import os


# Le chemin du dossier /res
RESOURCE_DIR: str = os.path.join(os.path.dirname(__file__), "..", "..", "res")
# Le chemin du dossier /res/dictionaries
DICTIONARIES_DIR: str = os.path.join(RESOURCE_DIR, "dictionaries")


# Le chemin du fichier /res/dictionaries/fr.txt
FRENCH_DICTIONARY_FILE: str = os.path.join(DICTIONARIES_DIR, "fr.txt")