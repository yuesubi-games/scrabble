import pygame

from assets import *
from scrapple import Letter

from .gui_letter import render_letter_to


class GuiDeck:
    def __init__(self, deck: list[Letter], position: pygame.Vector2) -> None:
        """
        Constructeur.
        :param deck: La main du joueur.
        :param position: La position de la main du joueur.
        :param unit: La taille d'une lettre.
        """
        self._letters: list[Letter] = deck.copy()
        self.position: pygame.Vector2 = position
    
    def is_position_above(self, global_position: pygame.Vector2) -> bool:
        """
        Vérifier si la position est au dessus du plateau.
        :param global_position: La position à vérifier.
        :return: True si la position est au dessus du plateau, False sinon.
        """
        local_pos = (global_position - self.position) / GUI_UNIT
        return 0 <= local_pos.y < 1 and 0 <= local_pos.x < len(self._letters)
     
    def peak_letter_at(self, global_position: pygame.Vector2) -> Letter | None:
        """
        Récupérer la lettre à la position donnée sans la retirer.
        :param global_position: La position à utiliser pour trouver la lettre.
        :return: La lettre récupérée ou None.
        """
        local_pos = (global_position - self.position) / GUI_UNIT

        letter = None
        if 0 <= local_pos.y < 1 and 0 <= local_pos.x < len(self._letters):
            letter = self._letters[int(local_pos.x)]
        
        return letter
    
    def pop_letter_at(self, global_position: pygame.Vector2) -> Letter | None:
        """
        Récupérer la lettre à la position donnée.
        :param global_position: La position à utiliser pour trouver la lettre.
        :return: La lettre récupérée ou None.
        """
        local_pos = (global_position - self.position) / GUI_UNIT

        letter = None
        if 0 <= local_pos.y < 1 and 0 <= local_pos.x < len(self._letters):
            letter = self._letters.pop(int(local_pos.x))
        
        return letter
    
    def push_letter(self, letter: Letter) -> None:
        """
        Ajouter une lettre à la main du joueur.
        :param letter: La lettre à ajouter.
        """
        self._letters.append(letter)

    def render_to(self, target: pygame.Surface) -> None:
        """
        Dessiner le plateau du scrapple.
        :param target: La surface sur laquelle dessiner.
        """
        for l, letter in enumerate(self._letters):
            pos = self.position + pygame.Vector2((l + 0.5) * GUI_UNIT,
                GUI_UNIT/2)
            if letter is not None:
                render_letter_to(target, letter, pos)