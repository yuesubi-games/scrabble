import time
import pygame

from .game_scene.gui_letter import *

from .event_manager import EventManager
from .scene_manager import Scene, SceneId, SceneManager

from .game_scene import GameScene
from .game_score_scene import GameScoreScene
from .menu_scene import MenuScene
from .bst_scene import BSTScene


FPS: float = 50.0


class App:
    def __init__(self) -> None:
        """Constructeur."""
        pygame.init()

        # Paramètres de la fenêtre
        pygame.display.set_caption("Scrapple")
        self.window = pygame.display.set_mode((640, 480))

        # Gestionnaire d'évènements
        self.event_mgr: EventManager = EventManager()

        # Gestionnaire de scène
        self.scene_mgr: SceneManager = SceneManager()
        self.scene_mgr.set_create_scene_callback(self.change_scene_callback)
        self.scene_mgr.switch_scene(SceneId.MENU)

    def __del__(self) -> None:
        """Destructeur."""
        pygame.quit()
    
    def change_scene_callback(self, scene_id: int, *scene_args, **scene_kwargs
            ) -> Scene | None:
        """
        Donne une instance de la scène qui correspond à l'identifiant.
        :param scene_id: L'identifiant du type de scène.
        :return: Si l'identifiant est correct une instance de Scene, sinon None.
        """
        new_scene = None
        
        if scene_id == SceneId.MENU:
            new_scene = MenuScene(*scene_args, **scene_kwargs)
        elif scene_id == SceneId.GAME:
            new_scene = GameScene(*scene_args, **scene_kwargs)
        elif scene_id == SceneId.BST:
            new_scene = BSTScene(*scene_args, **scene_kwargs)
        elif scene_id == SceneId.SCORE:
            new_scene = GameScoreScene(*scene_args, **scene_kwargs)
        
        return new_scene

    def run(self) -> None:
        """Lancer l'application."""

        # Variables de contrôle de temps
        new_time = time.perf_counter_ns()
        delta_time = 0.0
        last_time = new_time
        
        running = True

        while running:
            # Gestion des évènements
            self.event_mgr.update_events()
            if self.event_mgr.quit:
                running = False
            self.scene_mgr.register_events(self.event_mgr)

            # Actualisation
            self.scene_mgr.update_by(delta_time)

            # Rendu à l'écran
            self.window.fill((0, 0, 0))
            self.scene_mgr.render_to(self.window)
            pygame.display.flip()

            # Actualisation du temps
            new_time = time.perf_counter_ns()
            delta_time = (new_time - last_time) / 1e9
            last_time = new_time

            # Limiter le nombre de fps
            time_to_wait = (1 / FPS) - delta_time
            if time_to_wait > 0:
                time.sleep(time_to_wait)