"""Module avec toutes les types de lettre du scrapple."""

from .basic_letter import BasicLetter
from .joker_letter import JokerLetter
from .letter import Letter