class Tile:
    """Cellule du plateau du Scrapple."""

    def __init__(self, letter_coef: int = 1, word_coef: int = 1) -> None:
        """
        Constructeur du plateau.
        :param letter_coef: Le multiplicateur de score de la lettre qui est
            dessus.
        :param word_coef: Le multiplicateur de score du mot qui est dessus.
        """
        self.letter_coef: int = letter_coef
        self.word_coef: int = word_coef
        # True si les coefficients ont été utilisés, False sinon
        self.used = False